# -*- coding: utf-8 -*-
from sklearn.cluster import KMeans

from nltk.cluster.kmeans import KMeansClusterer
import nltk

"k = qtde de cluster"
def apply_kmeans(data,K,distance_measure=None):
    if(distance_measure=="cosine"):
        kclusterer = KMeansClusterer(K, distance=nltk.cluster.util.cosine_distance, repeats=25)
        assigned_clusters = kclusterer.cluster(data, assign_clusters=True)
        centroids = kclusterer.means()
    else: #euclidean distances
        kmeans = KMeans(n_clusters=K, random_state=0)
        kmeans.fit(data)
        centroids = kmeans.cluster_centers_
        assigned_clusters = kmeans.labels_
        a=kmeans.init
    return assigned_clusters,centroids




