
import os, datetime, time
from skimage import io
import numpy as np
import PIL
import torch
from torch.utils.data import Dataset
from torchvision import transforms


class ColorImageDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, img_names_list,labels, transform=None, transform_crop=None):

        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem          
        """
        self.root_dir = root_dir
        self.img_names_list = img_names_list
        self.labels = labels
        
        self.transform = transform
        self.transform_crop = transform_crop

    def __len__(self):
        return len(self.img_names_list)

    def __getitem__(self, idx):
      

        img_name = os.path.join(self.root_dir, self.img_names_list[idx]+'.tif')        
        
        image = io.imread(img_name)
        image = PIL.Image.fromarray(image)
        
        crops = image.copy() #crops sao os recortes sem transformacoes de rotacao, etc
        patches = image.copy()#patches sao os crops com transformacoes de rotacao, etc
        label = self.labels[idx]
                
#         image_name = self.img_names_list[idx]
       
        if self.transform_crop:
            crops, points = self.transform_crop(crops) #self.transform_crop pega a imagem original (PIL image) e faz um NRandomCrop, retorna uma list
            points =  np.asarray(points)
            patches = crops.copy()      #patches recebe um clone da lista de imagens (pil)      
            crops = torch.stack([transforms.ToTensor()(crop) for crop in crops]) #converto cada crop em um tensor
            
#             print('crops|patch',crops.shape)
            
        if self.transform:
          
            patches = torch.stack([ self.transform(patch) for patch in patches]) #aplico a cada crop as transformacoes, tranformando em tensor no final          
#             print(patches.shape)

        labels = [self.labels[idx] for i in range(patches.shape[0])]

        sample = {'crops': crops, 'patches': patches, 'label': np.asarray(labels), 'image_name': img_name, 'points': points}

    
        return sample
      
######################################################################
