import sys
import os
from utils import *
import argparse
# Params
parser = argparse.ArgumentParser(description='PyTorch ColorNet')
parser.add_argument('--model', default='ColorNet_resnet', type=str, help='choose a type of model')
args = parser.parse_args()

def load_errors(model_name):
  model_path = 'models/fc4/' + model_name + '/'
  if model_name.endswith('.pkl'):
    pkl = model_name
  else:
    # Find the last one
    fn = list(sorted(filter(lambda x: x.startswith('error'), os.listdir(model_path))))[-1]
    pkl = os.path.join(model_path, fn)
  with open(pkl) as f:
    return pickle.load(f)

def summary_angular_errors(errors):
  errors = sorted(errors)

  def g(f):
    return np.percentile(errors, f * 100)

  median = g(0.5)
  mean = np.mean(errors)
  trimean = 0.25 * (g(0.25) + 2 * g(0.5) + g(0.75))
  results = {
      '25': np.mean(errors[:int(0.25 * len(errors))]),
      '75': np.mean(errors[int(0.75 * len(errors)):]),
      '95': g(0.95),
      'tri': trimean,
      'med': median,
      'mean': mean
  }
  return results

def just_print_angular_errors(results):
  print ("25: %5.3f," % results['25'])
  print ("med: %5.3f" % results['med'])
  print ("tri: %5.3f" % results['tri'])
  print ("avg: %5.3f" % results['mean'])
  print ("75: %5.3f" % results['75'])
  print ("95: %5.3f" % results['95'])

def combine(models):
  combined = []
  for model in models:
    combined += load_errors(model)
  return combined

if __name__ == '__main__':

  save_dir = os.path.join('../mean_models', args.model)

  lst_errors = []
  tam = 0
  for i in range(1,4):

     lst_errors += scipy.io.loadmat(os.path.join(save_dir, 'folder' + str(i), 'lst_error' + str(i) + '.mat'))['lst_error'][0].tolist()

  lst_errors = np.asarray(lst_errors)
  results = summary_angular_errors(lst_errors)
  just_print_angular_errors(results)

  # i = 3
  # erros = scipy.io.loadmat(os.path.join(save_dir, 'folder' + str(i), 'lst_error' + str(i) + '.mat'))['lst_error'][0]
  # erros = sorted(erros)
  # print('Mean:', np.mean(erros), np.median(erros))
