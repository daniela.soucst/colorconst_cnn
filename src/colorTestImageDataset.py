import os
from torch.utils.data import Dataset
from skimage import io
import PIL
import torch
import numpy as np
######################################################################

class ColorTestImageDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, img_names_list, labels, transform=None, transform_crop=None):

        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem
        """
        self.root_dir = root_dir
        self.img_names_list = img_names_list
        self.labels = labels

        self.transform = transform
        self.transform_crop = transform_crop

    def __len__(self):
        return len(self.img_names_list)

    def __getitem__(self, idx):

        img_name = os.path.join(self.root_dir, self.img_names_list[idx] + '.tif')

        image = io.imread(img_name)
        image = PIL.Image.fromarray(image)

        if self.transform_crop:
            crops, _ = self.transform_crop(image)

        if self.transform:
            crops = torch.stack([self.transform(crop) for crop in
                                 crops])  # aplico a cada crop as transformacoes, tranformando em tensor no final

        list_labels = [self.labels[idx] for j in range(len(crops))]

        sample = {'images': crops, 'labels': np.asarray(list_labels)}

        return sample



######################################################################
