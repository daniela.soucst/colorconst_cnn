import numpy as np
import cv2
from PIL import Image
import sys
import os
from skimage import io

"""img_dir = data/Gehler_dataset/input/cc_output/8D5U5567.png
illu = np.array((0.526205, 0.35181, 0.121985))"""
def apply_illumination(img_dir, illu):
   image = cv2.imread(img_dir)
   image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

   image = (image / 255.0) ** 2.2 * 65536

   image_illum = np.power(image / 65535 * illu[None, None, :] / np.mean(illu), 1 / 2.2)[:, :, ::-1]

   return image_illum*255

'''image_dir = data/Gehler_dataset/input/8D5U5567.tif
rg = (ndarray) shape(3,)'''
def image_correction(image_dir, img_name, est, save_dir):
    im = cv2.imread(image_dir)
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)



    img = (im / 255.0) ** 2.2 * 65536

    corrected = np.power(img / 65535 / est[None, None, :] * np.mean(est), 1 / 2.2)[:, :, ::-1]

    # cv2.imwrite(os.path.join(save_dir, img_name+'.png'), corrected * 255)
    io.imsave(os.path.join(save_dir, img_name+'.png'), corrected * 255)
    return corrected*255

def generate_corrected_images(dataset_dir, est_file):

    save_dir = os.path.join(dataset_dir,'cc_output')
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)

    with open(est_file, 'r') as file:
        for line in file:
            line = line.strip().split()
            # print("Corrigindo imagem? ", line[0])
            est_r, est_g  = float(line[1]), float(line[2]),
            est_b = 1.0-(est_r+est_g)
            image_correction(os.path.join(dataset_dir, line[0]+'.tif'), line[0], np.array((est_r, est_g, est_b)),save_dir)


# if __name__ == '__main__':
#
#   if len(sys.argv) < 2:
#     print ("Ta faltando argumento")
#     exit(-1)
#   filename = __file__[2:]
#   dataset_dir = sys.argv[1]
#   est_file = sys.argv[2]
#   generate_corrected_images(dataset_dir, est_file)

# 0 [0.309353744949495, 0.3293761919191918]
# 1 [0.5294146202531644, 0.3151862025316454]
# 2 [0.44851692727272735, 0.3275946181818182]
# 3 [0.25093233333333337, 0.22689649999999997]

# illu = [0.309353744949495, 0.3293761919191918]
# # illu = [0.5294146202531644, 0.3151862025316454]
# # illu = [0.44851692727272735, 0.3275946181818182]
# # illu = [0.25093233333333337, 0.22689649999999997]
# # illu = [0.526205, 0.35181, 0.121985]
# img = apply_illumination("data/Gehler_dataset/input/cc_output/8D5U5567.png", np.array((illu[0],illu[1],1-(illu[0]+illu[1]))))
# cv2.imwrite("data/Gehler_dataset/8D5U5567_cluster0.png", img)