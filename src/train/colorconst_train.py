from __future__ import print_function, division
import os, time
import torch

import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
# Ignore warnings
import warnings
warnings.filterwarnings("ignore")
import scipy.io
import PIL
import utils

# from utils2 import progress_bar
# import clustering
import torch.nn as nn
import torch.optim as optim
# from model import ColorNet 
from colornet import ColorNet
from sklearn.model_selection import StratifiedKFold
import torch.backends.cudnn as cudnn
import argparse
import re
import glob
import random
from custom_transform import NRandomCrop
import light_estimation
plt.ion()   # interactive mode

parser = argparse.ArgumentParser(description='Color Constancy')
parser.add_argument('--lr', default=0.01, type=float, help='learning rate')
parser.add_argument('--epochs','-e', default=10, type=int, help='total de epocas')
parser.add_argument('--interval','-i', default=3, type=int, help='define de quantas em quantas epocas sera salvo o modelo ')
parser.add_argument('--resume', '-r', action='store_true', help='resume from checkpoint')
parser.add_argument('--seed', type=int, default=0, metavar='S', help='random seed (default: 1)') 
parser.add_argument('--model', default='ColorNet', type=str, help='the model name')
# parser.add_argument('--indexes_dir', default='/content/drive/My Drive/VISAO/data/Gehler_dataset/train_indexes', type=str, help='the model name')
parser.add_argument('--indexes_dir', default='data/Gehler_dataset/train_indexes', type=str, help='the model name')
parser.add_argument('--folder', type=int, default=1, metavar='S', help='random seed (default: 1)')
parser.add_argument('--batch_size', default=50, type=int, help='batch size')

args = parser.parse_args()


device = 'cuda' if torch.cuda.is_available() else 'cpu'
best_acc = 0  # best test accuracy
aux_save = 0 #var contadora 0..inter
last_epoch = 0 

# save_dir = os.path.join('colorconst_cnn/mean_models', args.model)
save_dir = os.path.join('mean_models', args.model)
if not os.path.exists(save_dir):
    os.mkdir(save_dir)
if not os.path.exists(os.path.join(save_dir,'folder' + str(args.folder))):
    os.mkdir(os.path.join(save_dir,'folder' + str(args.folder)))
    
random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed)
cudnn.benchmark=False
cudnn.deterministic=True


def worker_init(worker_id):
    random.seed(args.seed)
######################################################################

class ColorImageDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, img_names_list,labels, centroides, transform=None, transform_crop=None):

        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem          
        """
        self.root_dir = root_dir
        self.img_names_list = img_names_list
#         self.labels = labels
        self.list_illu = centroides #list(array())
        
        self.transform = transform
        self.transform_crop = transform_crop

    def __len__(self):
        return len(self.img_names_list)

    def __getitem__(self, idx):
      

        img_name = os.path.join(self.root_dir, self.img_names_list[idx]+'.png')
        
#         image = io.imread(img_name)        
#         image = PIL.Image.fromarray(image)
        
#         image = cv2.imread(img_name)
#         image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        
        list_crops = torch.FloatTensor()
        list_labels = []

        for i in range(len(self.list_illu)):
          
          image = light_estimation.apply_illumination(img_name, self.list_illu[i])          
          image = image.astype(np.uint8)           
          image = PIL.Image.fromarray(image)

          
          if self.transform_crop:       
            crops = self.transform_crop(image)            
            list_labels += [i for j in range(len(crops))]

          
          if self.transform:
              crops = torch.stack([ self.transform(crop) for crop in crops]) #aplico a cada crop as transformacoes, tranformando em tensor no final
              
          list_crops = torch.cat((list_crops,crops)) 

        # list_numpy = list_crops.cpu().numpy()
        # img = list_numpy[0]
        sample = {'images': list_crops, 'labels':np.asarray(list_labels)}

        return sample
      
######################################################################

def findLastCheckpoint(save_dir):
    file_list = glob.glob(os.path.join(save_dir,'folder' + str(args.folder), 'model_*.pth'))
    if file_list:
        epochs_exist = []
        for file_ in file_list:
            result = re.findall(".*model_(.*).pth.*", file_)
            epochs_exist.append(int(result[0]))
        initial_epoch = max(epochs_exist)
    else:
        initial_epoch = 0
    return initial_epoch
######################################################################

def split(img_name_list, labels):
  
  skf = StratifiedKFold(n_splits=3)
  train_name_list = []
  train_label_list = []
  test_name_list = []
  test_label_list = []

  for train, test in skf.split(img_name_list, labels):
      # print("%d %d" % (len(train), len(test)))
      train_names,train_labels = utils.get_names_and_labels_by_id(img_name_list, labels, train)
      train_name_list.append(train_names)
      train_label_list.append(train_labels)

      test_names, test_labels = utils.get_names_and_labels_by_id(img_name_list, labels, test) 
      test_name_list.append(test_names)
      test_label_list.append(test_labels)

      
  train_name_list = np.asarray(train_name_list)
  train_label_list = np.asarray(train_label_list)
  test_name_list = np.asarray(test_name_list)
  test_label_list = np.asarray(test_label_list)

#   print (train_name_list[0][0],train_label_list[0][0])    
  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/train_indexes','train_set'+str(1)+'.mat'),{'train_names':train_name_list[0],'train_labels':train_label_list[0]})
  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/test_indexes','test_set'+str(1)+'.mat'),{'test_names':test_name_list[0],'test_labels':test_label_list[0]})

  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/train_indexes','train_set'+str(2)+'.mat'),{'train_names':train_name_list[1],'train_labels':train_label_list[1]})
  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/test_indexes','test_set'+str(2)+'.mat'),{'test_names':test_name_list[1],'test_labels':test_label_list[1]})

  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/train_indexes','train_set'+str(3)+'.mat'),{'train_names':train_name_list[2],'train_labels':train_label_list[2]})
  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/test_indexes','test_set'+str(3)+'.mat'),{'test_names':test_name_list[2],'test_labels':test_label_list[2]})

######################################################################      


  
# Training
def training(epoch):
    print('\nEpoch: %d' % epoch)
    
    start_time = time.time()
    net.train()
    train_loss = 0
    correct = 0
    total = 0
    batch_loss = 0
           
    for batch_idx, sample_batched in enumerate(trainloader):
      
      inputs,targets = sample_batched['images'].to(device), sample_batched['labels'].to(device)
      
      for i in range(inputs.shape[0]):    

        patches = inputs[i]
        targets_patches = targets[i]
        
        optimizer.zero_grad()
        outputs = net(patches)              
        loss = criterion(outputs, targets_patches)
        loss.backward()
        optimizer.step()

        train_loss += loss.item()            
        _, predicted = outputs.max(1)
        total += targets_patches.size(0)
        correct += predicted.eq(targets_patches).sum().item()
        
      if batch_idx % 10 == 0:
          print('batch_idx: %4d | len(trainloader): %4d | Loss: %.3f | Acc: %.3f%% (%d/%d)' % (batch_idx, len(trainloader), train_loss/(batch_idx+1), 
                                                                                            100.*correct/total, correct, total)) 
       
        
    elapsed_time = time.time() - start_time
    print('>>>epoch = %4d | time = %2.4f | Loss: %.3f | Acc: %.3f%%'%(epoch,elapsed_time,train_loss/(batch_idx+1),100.*correct/total))
          
#     """Salvamos informacoes por epoca"""
#     torch.save(net, os.path.join(save_dir, 'folder' + str(args.folder),'model_%03d.pth' % (epoch+1)))
    
#     with open(os.path.join(save_dir,'folder' + str(args.folder), 'train_result.txt'), 'a') as f:          
#       np.savetxt(f, np.hstack((epoch+1, train_loss/(batch_idx+1), elapsed_time)), fmt='%2.10f')
#     with open(os.path.join(save_dir, 'folder' + str(args.folder), 'loss_result.txt'), 'a') as f:
#       np.savetxt(f, np.hstack(([ 100.*correct/total])), fmt='%3.5f')
      
         
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PREPARING DATA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

print('==> Preparing data..')


# gehler = {'root_dir': 'colorconst_cnn/data/Gehler_dataset/input/cc_output',#'root_dir': 'colorconst_cnn/data/Gehler_dataset/patches',
#           'img_names_labels':'colorconst_cnn/data/mean_shift_results/groundtruth_568_labels.txt',
#           'centroids': 'colorconst_cnn/data/mean_shift_results/groundtruth_568_centroids.txt'
#         }
gehler = {'root_dir': 'data/Gehler_dataset/input/cc_output',
          'img_names_labels':'data/mean_shift_results/groundtruth_568_labels.txt',
          'centroids': 'data/mean_shift_results/groundtruth_568_centroids.txt'
        }

img_name_list, labels = utils.get_names_and_labels(gehler['img_names_labels'])
centroids = utils.get_illu_centroids(gehler['centroids'])

# count = 1
# for img in img_name_list:
#   img_name = os.path.join(gehler['root_dir'], img+'.png')
#   print('imagem: ', count)
#   for i in range(len(centroids)):
#     image = light_estimation.apply_illumination(img_name, centroids[i])
#     # print(os.path.join(gehler['root_dir'],'illumination_'+str(i), img+'.png'),image.shape)
#     resu = cv2.imwrite(os.path.join(gehler['root_dir'],'illumination_'+str(i), img+'.png'), image)
#   count +=1
print(len(img_name_list),len(labels))
print(img_name_list[0],labels[0])


"""split(): Aqui geramos os indices para treinamento e teste"""
# split(img_name_list, labels)

# print(img_name_list[567], labels[567])

# average_color,std_color = utils.get_mean_stDeviation(gehler['root_dir'],img_name_list)

average_color = (0.28085261,0.2558284,0.21423263)
std_color = (0.19844685,0.18925277,0.18685587)


data_transform = transforms.Compose([ transforms.RandomRotation((-10,10)),transforms.RandomRotation((-5,5)),
        transforms.RandomRotation(0),transforms.RandomRotation(5),transforms.RandomRotation(10),
        # transforms.RandomCrop(227),
        transforms.RandomAffine(0, scale=(250,1000)),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(average_color, std_color),
    ])

crop_transform  = transforms.Compose(
    [NRandomCrop(size=227, n=5, padding=4),
#      transforms.Lambda(
#          lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),

     ])



train_names = scipy.io.loadmat(os.path.join(args.indexes_dir,'train_set'+str(args.folder)+'.mat'))['train_names']
train_labels = scipy.io.loadmat(os.path.join(args.indexes_dir,'train_set'+str(args.folder)+'.mat'))['train_labels'][0] #[[]]
# print (train_names[0], train_labels[0])
# print(len(train_names),len(train_labels))


train_dataset = ColorImageDataset(root_dir=gehler['root_dir'], img_names_list=train_names,
                                      labels=train_labels, centroides = centroids, transform=data_transform, transform_crop=crop_transform)

trainloader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=4, worker_init_fn=worker_init)


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BUILDING MODEL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
print('==> Building model..')

net = ColorNet(K=4,pretrain=True)

# net = models.alexnet(pretrained=True)
# net.classifier[6] =  nn.Linear(4096, 4)

# net = models.resnet18(pretrained=True)
# net.fc = nn.Linear(in_features=512, out_features=4, bias=True)
# print(net)
net = net.to(device)

"""negative log likelihood loss"""
criterion = nn.NLLLoss()
criterion = criterion.to(device)
# criterion = nn.CrossEntropyLoss()

# optimizer = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9, weight_decay=1e-4)
initial_epoch = findLastCheckpoint(save_dir=save_dir)  # load the last model in matconvnet style

if initial_epoch > 0:
    print('resuming by loading epoch %03d' % initial_epoch)
    # model.load_state_dict(torch.load(os.path.join(save_dir, 'model_%03d.pth' % initial_epoch)))
    model = torch.load(os.path.join(save_dir, 'folder' + str(args.folder), 'model_%03d.pth' % initial_epoch))
else:
  print('Comecando novo treino...')

optimizer = optim.SGD([{'params': net.features[0].parameters(),'lr': 10*args.lr},
                       {'params': net.features[4].parameters()},
                       {'params': net.features[8].parameters()},
                       {'params': net.features[10].parameters()},
                       {'params': net.features[12].parameters()},
                       {'params': net.classifier[0].parameters()},
                       {'params': net.classifier[3].parameters(),'lr': 10*args.lr},
                       {'params': net.classifier[6].parameters(),'lr': 10*args.lr}
                      ], lr=args.lr, momentum=0.9, weight_decay=5e-4)


for epoch in range(initial_epoch, args.epochs):
    #print('epoch',epoch)
    training(epoch)

    # print('>>>epoch = %4d | time = %2.4f'%(epoch,elapsed_time))