'''Train CIFAR10 with PyTorch.'''
from __future__ import print_function

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torch.backends.cudnn as cudnn
import torchvision.models as models

import torchvision
import torchvision.transforms as transforms
import os
import argparse
import glob,re, random
import numpy as np

import sys
sys.path.append("/media/HD21/dsc4/colorconst_cnn")
from colornet import ColorNet
import csv
from src.utils.selectCifar import *

parser = argparse.ArgumentParser(description='PyTorch CIFAR10 Training')
parser.add_argument('--lr', default=0.1, type=float, help='learning rate')
parser.add_argument('--epochs', '-e', default=10, type=int, help='total de epocas')
parser.add_argument('--resume', '-r', action='store_true', help='resume from checkpoint')
parser.add_argument('--folder', type=int, default=0, metavar='S', help='random seed (default: 1)')
parser.add_argument('--model', default='alex_cifar4', type=str, help='the model name')
parser.add_argument('--seed', type=int, default=0, metavar='S', help='random seed (default: 1)')

args = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'
best_acc = 0  # best test accuracy
aux_save = 0  # var contadora 0..intercs
last_epoch = 0

# save_dir = os.path.join('colorconst_cnn/mean_models', args.model)
save_dir = os.path.join('../../results/', args.model)
if not os.path.exists(save_dir):
    print(save_dir)
    os.mkdir(save_dir)
if not os.path.exists(os.path.join(save_dir, 'folder' + str(args.folder))):
    os.mkdir(os.path.join(save_dir, 'folder' + str(args.folder)))

random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed)
cudnn.benchmark = False
cudnn.deterministic = True


def worker_init(worker_id):
    random.seed(args.seed)

######################################################################

def findLastCheckpoint(save_dir):
    file_list = glob.glob(os.path.join(save_dir, 'folder' + str(args.folder), 'model_*.pth'))
    if file_list:
        epochs_exist = []
        for file_ in file_list:
            result = re.findall(".*model_(.*).pth.*", file_)
            epochs_exist.append(int(result[0]))
        initial_epoch = max(epochs_exist)
    else:
        initial_epoch = 0
    return initial_epoch
######################################################################

# Training
def train(epoch):
    print('\nEpoch: %d' % epoch)
    net.train()
    train_loss = 0
    correct = 0
    total = 0
    for batch_idx, (inputs, targets) in enumerate(trainloader):
        inputs, targets = inputs.to(device), targets.to(device)
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()

        train_loss += loss.item()
        _, predicted = outputs.max(1)
        total += targets.size(0)
        correct += predicted.eq(targets).sum().item()

        # progress_bar(batch_idx, len(trainloader), 'Loss: %.3f | Acc: %.3f%% (%d/%d)'
        #     % (train_loss/(batch_idx+1), 100.*correct/total, correct, total))
        if batch_idx % 10 == 0:
            print('batch_idx: %4d | len(testloader): %4d | Loss: %.3f | Acc: %.3f%% (%d/%d)' % (
                batch_idx, len(testloader), train_loss / (batch_idx + 1),
                100. * correct / total, correct, total))

    # elapsed_time = time.time() - start_time
    print('>>>epoch = %4d | Loss: %.3f | Acc: %.3f%%' % (
        epoch, train_loss / (batch_idx + 1), 100. * correct / total))

    # """Salvamos informacoes por epoca"""
    # if(epoch+1 == args.epochs):

from torchvision.datasets import CIFAR10

def test(epoch):
    global best_acc
    net.eval()
    test_loss = 0
    correct = 0
    total = 0
    with torch.no_grad():
        for batch_idx, (inputs, targets) in enumerate(testloader):
            inputs, targets = inputs.to(device), targets.to(device)
            outputs = net(inputs)
            loss = criterion(outputs, targets)

            test_loss += loss.item()
            _, predicted = outputs.max(1)
            total += targets.size(0)
            correct += predicted.eq(targets).sum().item()

            # progress_bar(batch_idx, len(testloader), 'Loss: %.3f | Acc: %.3f%% (%d/%d)'
            #     % (test_loss/(batch_idx+1), 100.*correct/total, correct, total))
            if batch_idx % 10 == 0:
                print('batch_idx: %4d | len(testloader): %4d | Loss: %.3f | Acc: %.3f%% (%d/%d)' % (
                    batch_idx, len(testloader), test_loss / (batch_idx + 1),
                    100. * correct / total, correct, total))

    print('>>>epoch = %4d | Loss: %.3f | Acc: %.3f%%' % (
        epoch, test_loss / (batch_idx + 1), 100. * correct / total))
    test_loss /= len(testloader.dataset)

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(testloader.dataset), 100. * correct / len(testloader.dataset)))

    with open(os.path.join(save_dir, 'folder' + str(args.folder), 'test_acc.csv'),
              "a") as my_csv:  # writing the file as my_csv
        csvWriter = csv.writer(my_csv, delimiter=',')  # using the csv module to write the file
        csvWriter.writerow([epoch + 1, 100. * correct / total])

    # Save checkpoint.
    acc = 100.*correct/total
    if acc > best_acc:
        # print('Saving..')
        # state = {
        #     'net': net.state_dict(),
        #     'acc': acc,
        #     'epoch': epoch,
        # }
        # if not os.path.isdir('checkpoint'):
        #     os.mkdir('checkpoint')
        # torch.save(state, './checkpoint/ckpt.t7')
        best_acc = acc
        torch.save(net, os.path.join(save_dir, 'folder' + str(args.folder), 'model_%03d.pth' % (epoch + 1)))


# Data
print('==> Preparing data..')
# transform_train = transforms.Compose([
#     # transforms.RandomCrop(32, padding=4),
#     transforms.RandomResizedCrop(224),
#     transforms.RandomHorizontalFlip(),
#     transforms.ToTensor(),
#     transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
# ])
#
# transform_test = transforms.Compose([
#     transforms.RandomResizedCrop(224),
#     transforms.ToTensor(),
#     transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)),
# ])

# Downloading/Louding CIFAR10 data

# Transformations
# RC   = transforms.RandomCrop(32, padding=4)
RC   = transforms.RandomResizedCrop(227)
RHF  = transforms.RandomHorizontalFlip()
RVF  = transforms.RandomVerticalFlip()
NRM  = transforms.Normalize((0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010))
TT   = transforms.ToTensor()
TPIL = transforms.ToPILImage()

# Transforms object for trainset with augmentation
transform_with_aug = transforms.Compose([TPIL, RC, RHF, TT, NRM])
# Transforms object for testset with NO augmentation
transform_no_aug   = transforms.Compose([TPIL, RC, TT, NRM])

trainset  = CIFAR10(root='./data', train=True , download=True)#, transform = transform_with_aug)
testset   = CIFAR10(root='./data', train=False, download=True)#, transform = transform_no_aug)
classDict = {'plane':0, 'car':1, 'bird':2, 'cat':3, 'deer':4, 'dog':5, 'frog':6, 'horse':7, 'ship':8, 'truck':9}

# Separating trainset/testset data/label
x_train  = trainset.train_data
x_test   = testset.test_data
y_train  = trainset.train_labels
y_test   = testset.test_labels

# Let's choose cats (class 3 of CIFAR) and dogs (class 5 of CIFAR) as trainset/testset
cat_dog_trainset = \
    DatasetMaker(
        [get_class_i(x_train, y_train, classDict['cat']), get_class_i(x_train, y_train, classDict['dog']),
         get_class_i(x_train, y_train, classDict['bird']),get_class_i(x_train, y_train, classDict['deer'])],
        transform_with_aug
    )
cat_dog_testset  = \
    DatasetMaker(
        [get_class_i(x_test , y_test , classDict['cat']), get_class_i(x_test , y_test , classDict['dog']),
         get_class_i(x_test, y_test, classDict['bird']),get_class_i(x_test, y_test, classDict['deer'])],
        transform_no_aug
    )


trainloader   = DataLoader(cat_dog_trainset, batch_size=64, shuffle=True , num_workers=2, worker_init_fn=worker_init)
testloader    = DataLoader(cat_dog_testset , batch_size=64, shuffle=False, num_workers=2, worker_init_fn=worker_init)

# Model
print('==> Building model..')

# net = ColorNet(K=4, pretrain=True)
# net = models.resnet18()
# net.fc = nn.Linear(in_features=512, out_features=4, bias=True)

net = models.alexnet(pretrained=True)
net.classifier[6] =  nn.Linear(4096, 4)
net = net.to(device)


criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9, weight_decay=5e-4)

initial_epoch = findLastCheckpoint(save_dir=save_dir)  # load the last model in matconvnet style

if initial_epoch > 0:
    print('resuming by loading epoch %03d' % initial_epoch)
    # model.load_state_dict(torch.load(os.path.join(save_dir, 'model_%03d.pth' % initial_epoch)))
    net = torch.load(os.path.join(save_dir, 'folder' + str(args.folder), 'model_%03d.pth' % initial_epoch))
else:
    print('Comecando novo treino...')

for epoch in range(initial_epoch, args.epochs):
    train(epoch)
    test(epoch)
