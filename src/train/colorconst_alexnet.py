from __future__ import print_function, division
import os, time
import torch

from skimage import io
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
# Ignore warnings
import warnings

warnings.filterwarnings("ignore")
import scipy.io
import PIL
import torchvision.models as models
# from utils2 import progress_bar
# import clustering
import torch.nn as nn
import torch.optim as optim
# from model import ColorNet

from sklearn.model_selection import StratifiedKFold
import torch.backends.cudnn as cudnn
import argparse
import re
import glob
import random
import csv

import sys
sys.path.append("/media/HD21/dsc4/colorconst_cnn")
from src import light_estimation
from src.colorTestImageDataset import ColorTestImageDataset
from src.utils import util as utils
from colornet import ColorNet
from custom_transform import NRandomCrop
plt.ion()  # interactive mode

parser = argparse.ArgumentParser(description='Color Constancy')
parser.add_argument('--lr', default=0.1, type=float, help='learning rate')
parser.add_argument('--epochs', '-e', default=10, type=int, help='total de epocas')
parser.add_argument('--resume', '-r', action='store_true', help='resume from checkpoint')
parser.add_argument('--seed', type=int, default=0, metavar='S', help='random seed (default: 1)')
parser.add_argument('--model', default='ColorNet_alex_basic', type=str, help='the model name')
# parser.add_argument('--indexes_dir', default='/content/drive/My Drive/VISAO/data/Gehler_dataset/train_indexes', type=str, help='the model name')
parser.add_argument('--indexes_dir', default='../../data/Gehler_dataset/train_indexes', type=str, help='nomes das imagens de treinamento')
parser.add_argument('--indexes_test_dir', default='../../data/Gehler_dataset/test_indexes', type=str, help='nomes das imagens de teste')
parser.add_argument('--folder', type=int, default=1, metavar='S', help='random seed (default: 1)')
parser.add_argument('--batch_size', default=25, type=int, help='batch size')

args = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'
best_acc = 0  # best test accuracy
aux_save = 0  # var contadora 0..inter
last_epoch = 0

# save_dir = os.path.join('colorconst_cnn/mean_models', args.model)
save_dir = os.path.join('../../results/', args.model)
if not os.path.exists(save_dir):
    os.mkdir(save_dir)
if not os.path.exists(os.path.join(save_dir, 'folder' + str(args.folder))):
    os.mkdir(os.path.join(save_dir, 'folder' + str(args.folder)))

random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed)
cudnn.benchmark = False
cudnn.deterministic = True


def worker_init(worker_id):
    random.seed(args.seed)


######################################################################

class ColorImageDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, img_names_list, labels, centroides, transform=None, transform_crop=None, real_images=False):

        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem
        """
        self.root_dir = root_dir
        self.img_names_list = img_names_list
        self.labels = labels
        self.list_illu = centroides  # list(array())

        self.transform = transform
        self.transform_crop = transform_crop
        self.real_images = real_images


    def __len__(self):
        return len(self.img_names_list)

    def __getitem__(self, idx):

        if(self.real_images):

            img_name = os.path.join(self.root_dir, self.img_names_list[idx] + '.tif')

            image = io.imread(img_name)
            image = PIL.Image.fromarray(image)

            # if self.transform_crop:
            #     crops, _ = self.transform_crop(image)

            if self.transform:
                image = self.transform(image)#aplico a cada crop as transformacoes, tranformando em tensor no final

            # list_labels = [labels[idx] for j in range(len(crops))]
            label = self.labels[idx]
            sample = {'image': image, 'label': label}
        else:
            """"Geracao das imagens sinteticas para treinamento"""
            original_name = self.img_names_list[idx][:-4]

            img_dir = os.path.join(self.root_dir, original_name, self.img_names_list[idx] + '.png')
            label = self.labels[idx]

            for i in range(len(self.list_illu)):

                # image = io.imread(img_name)
                # image = PIL.Image.fromarray(image)
                image = light_estimation.apply_illumination(img_dir, self.list_illu[i])
                image = PIL.Image.fromarray(image.astype(np.uint8))

                if self.transform:
                    image = self.transform(image)

                # list_crops = torch.cat((list_crops, crops))

            sample = {'image': image, 'label': label}

        return sample



######################################################################

def findLastCheckpoint(save_dir):
    file_list = glob.glob(os.path.join(save_dir, 'folder' + str(args.folder), 'model_*.pth'))
    if file_list:
        epochs_exist = []
        for file_ in file_list:
            result = re.findall(".*model_(.*).pth.*", file_)
            epochs_exist.append(int(result[0]))
        initial_epoch = max(epochs_exist)
    else:
        initial_epoch = 0
    return initial_epoch


######################################################################

def split(img_name_list, labels):
    skf = StratifiedKFold(n_splits=3)
    train_name_list = []
    train_label_list = []
    test_name_list = []
    test_label_list = []

    for train, test in skf.split(img_name_list, labels):
        # print("%d %d" % (len(train), len(test)))
        train_names, train_labels = utils.get_names_and_labels_by_id(img_name_list, labels, train)
        train_name_list.append(train_names)
        train_label_list.append(train_labels)

        test_names, test_labels = utils.get_names_and_labels_by_id(img_name_list, labels, test)
        test_name_list.append(test_names)
        test_label_list.append(test_labels)

    train_name_list = np.asarray(train_name_list)
    train_label_list = np.asarray(train_label_list)
    test_name_list = np.asarray(test_name_list)
    test_label_list = np.asarray(test_label_list)

    #   print (train_name_list[0][0],train_label_list[0][0])
    scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/train_indexes', 'train_set' + str(1) + '.mat'),
                     {'train_names': train_name_list[0], 'train_labels': train_label_list[0]})
    scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/test_indexes', 'test_set' + str(1) + '.mat'),
                     {'test_names': test_name_list[0], 'test_labels': test_label_list[0]})

    scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/train_indexes', 'train_set' + str(2) + '.mat'),
                     {'train_names': train_name_list[1], 'train_labels': train_label_list[1]})
    scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/test_indexes', 'test_set' + str(2) + '.mat'),
                     {'test_names': test_name_list[1], 'test_labels': test_label_list[1]})

    scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/train_indexes', 'train_set' + str(3) + '.mat'),
                     {'train_names': train_name_list[2], 'train_labels': train_label_list[2]})
    scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/test_indexes', 'test_set' + str(3) + '.mat'),
                     {'test_names': test_name_list[2], 'test_labels': test_label_list[2]})


######################################################################


# Training
def training(epoch, trainloader):
    print('\nEpoch: %d' % epoch)

    start_time = time.time()
    net.train()
    train_loss = 0
    correct = 0
    total = 0
    batch_loss = 0

    for batch_idx, sample_batched in enumerate(trainloader):

        inputs, targets = sample_batched['image'].to(device), sample_batched['label'].to(device)

        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()

        train_loss += loss.item()
        _, predicted = outputs.max(1)
        total += targets.size(0)
        correct += predicted.eq(targets).sum().item()



        if batch_idx % 10 == 0:
            print('batch_idx: %4d | len(trainloader): %4d | Loss: %.3f | Acc: %.3f%% (%d/%d)' % (
                batch_idx, len(trainloader), train_loss / (batch_idx + 1),
                100. * correct / total, correct, total))

    elapsed_time = time.time() - start_time
    print('>>>epoch = %4d | time = %2.4f | Loss: %.3f | Acc: %.3f%%' % (
    epoch, elapsed_time, train_loss / (batch_idx + 1), 100. * correct / total))


    """Salvamos informacoes por epoca"""
    # if(epoch+1 == args.epochs):
    torch.save(net, os.path.join(save_dir, 'folder' + str(args.folder),'model_%03d.pth' % (epoch+1)))



    with open(os.path.join(save_dir,'folder' + str(args.folder), 'train_loss.csv'), "a") as my_csv:  # writing the file as my_csv
        csvWriter = csv.writer(my_csv, delimiter=',')  # using the csv module to write the file
        csvWriter.writerow([epoch+1, train_loss/(batch_idx+1), elapsed_time])

    with open(os.path.join(save_dir,'folder' + str(args.folder), 'train_accuracy.csv'), "a") as my_csv:  # writing the file as my_csv
        csvWriter = csv.writer(my_csv, delimiter=',')  # using the csv module to write the file
        csvWriter.writerow([epoch+1, 100.*correct/total])

"""-----------------------------TESTE----------------------------------"""
def test(epoch, testloader, network):

    start_time = time.time()
    network.eval()
    test_loss = 0
    correct = 0
    total = 0
    
    with torch.no_grad():
        for n_count, batch_yx in enumerate(testloader):
            inputs, targets = batch_yx['images'].to(device), batch_yx['labels'].to(device)

            # print(inputs.shape, targets.shape)
            # outputs = net(inputs)
            for i in range(inputs.shape[0]):
                patches = inputs[i]
                targets_patches = targets[i]

                outputs = network(patches)
                loss = criterion(outputs, targets_patches)
                # loss.backward()
                # optimizer.step()

                test_loss += loss.item()
                _, predicted = outputs.max(1)
                total += targets_patches.size(0)
                correct += predicted.eq(targets_patches).sum().item()

            if n_count % 10 == 0:
                print('batch_idx: %4d | len(testloader): %4d | Loss: %.3f | Acc: %.3f%% (%d/%d)' % (
                    n_count, len(testloader), test_loss / (n_count + 1),
                    100. * correct / total, correct, total))

    elapsed_time = time.time() - start_time
    print('>>>epoch = %4d | time = %2.4f | Loss: %.3f | Acc: %.3f%%' % (
        epoch, elapsed_time, test_loss / (n_count + 1), 100. * correct / total))
   
    test_loss /= len(testloader.dataset)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(
        test_loss, correct, len(testloader.dataset), 100. * correct / len(testloader.dataset)))
   
    with open(os.path.join(save_dir,'folder' + str(args.folder), 'test_acc.csv'), "a") as my_csv:  # writing the file as my_csv
        csvWriter = csv.writer(my_csv, delimiter=',')  # using the csv module to write the file
        csvWriter.writerow([epoch+1,100.*correct/total])

"""--------------------------------------------------------------------------------"""

""">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PREPARING DATA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"""

print('==> Preparing data..')

# gehler = {'root_dir': 'colorconst_cnn/data/Gehler_dataset/input/cc_output',#'root_dir': 'colorconst_cnn/data/Gehler_dataset/patches',
#           'img_names_labels':'colorconst_cnn/data/mean_shift_results/groundtruth_568_labels.txt',
#           'centroids': 'colorconst_cnn/data/mean_shift_results/groundtruth_568_centroids.txt'
#         }
gehler = {'root_dir': '/media/HD21/dsc4/input/patches',
          'real_dir': '/media/HD21/dsc4/input/original',
          'img_names_labels': '../../data/Gehler_dataset/mean_shift_results/groundtruth_568_labels.txt',
          'centroids': '../../data/Gehler_dataset/mean_shift_results/groundtruth_568_centroids.txt'
          }

img_name_list, labels = utils.get_names_and_labels(gehler['img_names_labels'])
centroids = utils.get_illu_centroids(gehler['centroids'])

train_names = scipy.io.loadmat(os.path.join(args.indexes_dir, 'train_set' + str(args.folder) + '.mat'))['train_names']
train_labels =scipy.io.loadmat(os.path.join(args.indexes_dir, 'train_set' + str(args.folder) + '.mat'))['train_labels'][0]  # [[]]

test_names = scipy.io.loadmat(os.path.join(args.indexes_test_dir, 'test_set' + str(args.folder) + '.mat'))['test_names']
test_labels =scipy.io.loadmat(os.path.join(args.indexes_test_dir, 'test_set' + str(args.folder) + '.mat'))['test_labels'][0]  # [[]]

# print (train_names[0], train_labels[0])
# print(len(train_names),len(train_labels))

# train_names,train_labels = utils.get_names_and_labels_patches(train_names, train_labels, 50)
#test_names,test_labels = utils.get_names_and_labels_patches(train_names, train_labels, 200)

# count = 1
# for img in img_name_list:
#   img_name = os.path.join(gehler['root_dir'], img+'.png')
#   print('imagem: ', count)
#   for i in range(len(centroids)):
#     image = light_estimation.apply_illumination(img_name, centroids[i])
#     # print(os.path.join(gehler['root_dir'],'illumination_'+str(i), img+'.png'),image.shape)
#     resu = cv2.imwrite(os.path.join(gehler['root_dir'],'illumination_'+str(i), img+'.png'), image)
#   count +=1


"""split(): Aqui geramos os indices para treinamento e teste"""
# split(img_name_list, labels)

# print(img_name_list[567], labels[567])

# average_color,std_color = utils.get_mean_stDeviation(gehler['root_dir'],img_name_list)

average_color = (0.28085261, 0.2558284, 0.21423263)
std_color = (0.19844685, 0.18925277, 0.18685587)

data_transform = transforms.Compose([transforms.RandomRotation((-10, 10)), transforms.RandomRotation((-5, 5)),
                                     transforms.RandomRotation(0), transforms.RandomRotation(5),
                                     transforms.RandomRotation(10),
                                     transforms.RandomCrop(227),
                                     transforms.RandomAffine(0, scale=(250, 1000)),
                                     transforms.RandomHorizontalFlip(),
                                     transforms.ToTensor(),
                                     transforms.Normalize(average_color, std_color),
                                     ])
transform_test = transforms.Compose([
    transforms.RandomCrop(227),
    transforms.ToTensor(),
    transforms.Normalize(average_color, std_color),
])

crop_transform = transforms.Compose(
    [NRandomCrop(size=227, n=10, padding=4),
     #      transforms.Lambda(
     #          lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),

     ])


train_dataset = ColorImageDataset(root_dir=gehler['root_dir'], img_names_list=train_names,
                                  labels=train_labels, centroides=centroids, transform=data_transform,
                                  transform_crop=None)

trainloader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=2,
                         worker_init_fn=worker_init)
#----------------------------------------

train_dataset_real = ColorImageDataset(root_dir=gehler['real_dir'], real_images=True, img_names_list=train_names,
                                  labels=train_labels, centroides=centroids, transform=data_transform,
                                  transform_crop=None)

trainloader_real = DataLoader(train_dataset_real, batch_size=args.batch_size, shuffle=True, num_workers=4,
                         worker_init_fn=worker_init)

#----------------------------------------
test_dataset = ColorTestImageDataset(root_dir=gehler['real_dir'], img_names_list=test_names,
                                 labels=test_labels, transform=transform_test, transform_crop=crop_transform)

testloader = DataLoader(test_dataset, batch_size=100, shuffle=True, num_workers=4)

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BUILDING MODEL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
print('==> Building model..')

# net = ColorNet(K=4, pretrain=False)

net = models.alexnet(pretrained=True)
net.classifier[6] =  nn.Linear(4096, 4)
net = net.to(device)
# net = models.resnet18(pretrained=True)
# net.fc = nn.Linear(in_features=512, out_features=4, bias=True)

# net = net.to(device)

"""negative log likelihood loss"""
criterion = nn.NLLLoss()
# criterion = nn.CrossEntropyLoss()
criterion = criterion.to(device)

# optimizer = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9, weight_decay=1e-4)
optimizer = optim.SGD([{'params': net.features[0].parameters(),'lr': 10*args.lr},
                        {'params': net.features[4].parameters()},
                        {'params': net.features[8].parameters()},
                        {'params': net.features[10].parameters()},
                        {'params': net.features[12].parameters()},
                        {'params': net.classifier[0].parameters()},
                        {'params': net.classifier[3].parameters(),'lr': 10*args.lr},
                       {'params': net.classifier[6].parameters(),'lr': 10*args.lr}
                       ], lr=args.lr, momentum=0.9, weight_decay=5e-4)

initial_epoch = findLastCheckpoint(save_dir=save_dir)  # load the last model in matconvnet style

if initial_epoch > 0:
    print('resuming by loading epoch %03d' % initial_epoch)
    # model.load_state_dict(torch.load(os.path.join(save_dir, 'model_%03d.pth' % initial_epoch)))
    net = torch.load(os.path.join(save_dir, 'folder' + str(args.folder), 'model_%03d.pth' % initial_epoch))
else:
    print('Comecando novo treino...')



for epoch in range(initial_epoch, args.epochs):

   # training(epoch, trainloader=trainloader)
   training(epoch, trainloader=trainloader_real)
   print("Teste")
   test(epoch, testloader, network=net)

# test(1, testloader, network=net2)

# for i in range(1,11):
#   initial_epoch = i
#   print('resuming by loading epoch %03d' % initial_epoch)
#   net = torch.load(os.path.join(save_dir, 'folder' + str(args.folder), 'model_%03d.pth' % initial_epoch))
#   test(i, testloader, network=net)

# for epoch in range(initial_epoch, args.epochs):
#     training(epoch, trainloader=trainloader_real) #treinando com imagens reais
