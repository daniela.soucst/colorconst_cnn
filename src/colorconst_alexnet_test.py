# -*- coding: utf-8 -*-
import argparse
import torch
from colornet import ColorNet
import os, glob, datetime, time
from skimage import io, transform
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
from torchvision import models
import torch.backends.cudnn as cudnn
import torch.nn as nn
import torch.optim as optim
import utils
import scipy.io
import scipy.optimize
import PIL
# Ignore warnings
import warnings
import numpy as np
import re
import math
import torch.nn.functional as F
from custom_transform import NRandomCrop

warnings.filterwarnings("ignore")

device = 'cuda' if torch.cuda.is_available() else 'cpu'


######################################################################

class ColorImageDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, img_names_list, labels, transform=None, transform_crop=None):

        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem
        """
        self.root_dir = root_dir
        self.img_names_list = img_names_list
        self.labels = labels
        #         self.list_illu = centroides  # list(array())

        self.transform = transform
        self.transform_crop = transform_crop

    def __len__(self):
        return len(self.img_names_list)

    def __getitem__(self, idx):

        img_name = os.path.join(self.root_dir, self.img_names_list[idx] + '.tif')
        image = io.imread(img_name)
        image = PIL.Image.fromarray(image)

        crops = []
        if self.transform_crop:
            crops, _ = self.transform_crop(image)

        if self.transform:
            crops = torch.stack([self.transform(crop) for crop in crops])

        label = [labels[idx] for j in range(len(crops))]

        sample = {'image': crops, 'label': np.asarray(label)}

        return sample


# Params
parser = argparse.ArgumentParser(description='PyTorch ColorNet')
parser.add_argument('--model', default='ColorNet_alexnetSame', type=str, help='choose a type of model')
parser.add_argument('--model_name', default='model_015.pth', type=str, help='the model name')
parser.add_argument('--batch_size', default=100, type=int, help='batch size')
# parser.add_argument('--train_data', default='/content/gdrive/My Drive/VISAO/data/Gehler_dataset/patches', type=str,
#                     help='path of train data')
# parser.add_argument('--epoch', default=10, type=int, help='number of train epoches')
parser.add_argument('--indexes_dir', default='../data/Gehler_dataset/test_indexes',
                    type=str, help='the model name')
parser.add_argument('--folder', type=int, default=1, metavar='S', help='random seed (default: 1)')
# parser.add_argument('--model_dir', default='/content/gdrive/My Drive/VISAO/checkpoint', help='directory of the model')
parser.add_argument('--lr', default=0.1, type=float, help='initial learning rate for Adam')
args = parser.parse_args()

batch_size = args.batch_size
cuda = torch.cuda.is_available()

# save_dir = os.path.join(args.model_dir, args.model+'_' + str(n_epoch))
# save_dir = os.path.join('/content/gdrive/My Drive/VISAO/checkpoint', args.model+str(args.folder)+'.pth')
save_dir = os.path.join('../mean_models', args.model, 'folder' + str(args.folder))
# save_dir = os.path.join('colorconst_cnn/mean_models', args.model,'folder' + str(args.folder))
print('save_dir:', save_dir)

# >>>>>>>>>>>>>>>>>. Preparing data
gehler = {'root_dir': '../data/Gehler_dataset/input',
          'img_names_labels': '../data/mean_shift_results/groundtruth_568_labels.txt',
          'centroids': '../data/mean_shift_results/groundtruth_568_centroids.txt',
          'real_illu_dir': '../data/Gehler_dataset/srgb8bit_whitepoints.txt',
          }

img_name_list, labels = utils.get_names_and_labels(gehler['img_names_labels'])

real_illu = utils.get_allGroundtruth(
    (gehler['real_illu_dir']))  # variavel global que cont�m os centros dos clusters de ilumina��o
# average_color,std_color = utils.get_mean_stDeviation(gehler['root_dir'],img_name_list)
average_color = (0.28085261, 0.2558284, 0.21423263)
std_color = (0.19844685, 0.18925277, 0.18685587)

# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BUILDING MODEL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
# model selection
print('===> Building model')
# net = ColorNet(K=4)

net = models.resnet18(pretrained=True)
net.fc = nn.Linear(in_features=512, out_features=4, bias=True)

net = net.to(device)

# if device == 'cuda':
#     net = torch.nn.DataParallel(net)
#     cudnn.benchmark = True

if not os.path.exists(os.path.join(save_dir, args.model_name)):

    print('Modelo nao encontrado: ', os.path.join(save_dir, args.model_name))
else:
    # model.load_state_dict(torch.load(os.path.join(args.model_dir, args.model_name)))
    net = torch.load(os.path.join(save_dir, args.model_name))
    print('load trained model: ', os.path.join(save_dir, args.model_name))


# net = net.to(device)
def test(testloader):
    net.eval()
    test_loss = 0
    correct = 0
    weights_list = []
    list_output = []

    with torch.no_grad():
        for n_count, batch_yx in enumerate(testloader):
            inputs, targets = batch_yx['image'].to(device), batch_yx['label'].to(device)

            # print(inputs.shape, targets.shape)
            # outputs = net(inputs)
            for i in range(inputs.shape[0]):
                patches = inputs[i]
                # targets_patches = targets[i]

                outputs = net(patches)
                # outputs = F.softmax(outputs, dim=1)
                weights_list = outputs.mean(0)
                # sum = weights_list.sum()
                list_output.append(weights_list.cpu().data.numpy())
        #             weights_list = outputs.mean(0)

        #         return weights_list.cpu()
        return list_output


#######################################################
transform_test = transforms.Compose([
    transforms.RandomCrop(227),
    transforms.ToTensor(),
    transforms.Normalize(average_color, std_color),
])

crop_transform = transforms.Compose(
    [NRandomCrop(size=227, n=20, padding=4),
     #      transforms.Lambda(
     #          lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),

     ])

test_names = scipy.io.loadmat(os.path.join(args.indexes_dir, 'test_set' + str(args.folder) + '.mat'))['test_names']
test_labels = scipy.io.loadmat(os.path.join(args.indexes_dir, 'test_set' + str(args.folder) + '.mat'))['test_labels'][
    0]  # [[]]
print("test_names: ", test_names[0], test_labels[0])
print("test_names shapes: ", test_names.shape, test_labels.shape)
# test_names, test_labels, real_illu = utils.get_names_and_labels_patches(test_names, test_labels, real_illu, 200)


list_gd = []
for name in test_names:
    list_gd.append(utils.get_groundtruth_byImage(gehler['real_illu_dir'], name))

centroids = utils.get_illu_centroids(gehler['centroids'])
centroids = np.asarray(centroids)
lst_error = []

test_dataset = ColorImageDataset(root_dir=gehler['root_dir'], img_names_list=test_names,
                                 labels=test_labels, transform=transform_test, transform_crop=crop_transform)

testloader = DataLoader(test_dataset, batch_size=args.batch_size, shuffle=True, num_workers=4)

batch_weights = test(testloader)
batch_weights = np.asarray(batch_weights)
print('weights_list :', batch_weights.shape)

list_est = []

for k in range(batch_weights.shape[0]):
    e_est = [0., 0., 0.]
    for l in range(len(centroids)):
        e_est[0] += centroids[l][0] * batch_weights[k][l]
        e_est[1] += centroids[l][1] * batch_weights[k][l]
        e_est[2] += centroids[l][2] * batch_weights[k][l]
    # print(len(e_est))
    list_est.append(e_est)

# print(np.asarray(list_est).shape)
# print(list_est)
list_errors = np.asarray(utils.get_errors(list_est, list_gd))
scipy.io.savemat(os.path.join(save_dir, 'lst_error' + str(args.folder) + '.mat'), {'lst_error': list_errors})
