# -*- coding: utf-8 -*-
import os
import numpy as np
import cv2
import glob
from matplotlib import pyplot as plt
import scipy.io

img_directory = 'data/Gehler_dataset/'
# img_directory = 'drive/VISAO/data/'

#saaaa
def get_rg_chromaticity(database_name='groundtruth_568'):
    rg_chromaticity = []
    img_names = []
    print("Loading and shuffle fn_and_illum[]")
    if (database_name == 'groundtruth_568'):
        # ground_truth = scipy.io.loadmat(img_directory + database_name + '/real_illum_568..mat')['real_rgb']  # (568,3)
        # ground_truth /= np.linalg.norm(ground_truth, axis=1)[..., np.newaxis]  # normalizando
        # # print type(ground_truth), ground_truth.shape
        # rg_chromaticity = np.copy(ground_truth)
        # rg_chromaticity = np.delete(rg_chromaticity, 2, 1)
        file = open("data/Gehler_dataset/srgb8bit_whitepoints.txt", "r")
        for line in file:
            r = float(line.split()[1])
            g = float(line.split()[2])
            rg_chromaticity.append([r,g])
            img_names.append(line.split()[0])

    if (database_name == 'Gray_Ball'):
        ground_truth = scipy.io.loadmat(img_directory + database_name + '/real_illum_11346.mat')['real_rgb']
        ground_truth /= np.linalg.norm(ground_truth, axis=1)[..., np.newaxis]  # normalizando
        # print type(ground_truth), ground_truth.shape
        rg_chromaticity = np.copy(ground_truth)
        rg_chromaticity = np.delete(rg_chromaticity, 2, 1)

    return rg_chromaticity,img_names


# listGehler = scipy.io.loadmat('data/Gehler_dataset/groundtruth_568/listGehler.mat')
# print (listGehler)
# get_rg_chromaticity()