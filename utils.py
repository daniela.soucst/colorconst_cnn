import cv2
import numpy as np
import scipy
from matplotlib import pyplot as plt
from skimage import io, transform
from random import randint
from sklearn.feature_extraction.image import PatchExtractor
import scipy.io
import glob
import os
import sys
import time
import math


def get_mean_stDeviation(img_dir,img_names_list):

    average_color = np.array([0.,0.,0.])
    std_color = np.array([0.,0.,0.])
    for k in range(0,len(img_names_list)):
        img_name = img_names_list[k]
        # end = img_name.find('.png', 0) #tirar o .png
        # img_name = img_name[0:end]

#         img = cv2.cvtColor(cv2.imread(img_dir+"/"+img_name+'.tif'),cv2.COLOR_BGR2RGB)
#        img = io.imread(img_dir+"/"+img_name+'.tif')
        img = io.imread(img_name)
        average_color_local = [img[:, :, i].mean() for i in range(img.shape[-1])]
        average_color = np.add(average_color,average_color_local)
        std_color = np.add(std_color, [img[:, :, j].std() for j in range(img.shape[-1])])


    total_images = len(img_names_list)
    average_color = np.asarray(average_color) / [255.0*total_images, 255.0*total_images, 255.0*total_images]
    std_color = np.asarray(std_color) / [255.0*total_images, 255.0*total_images, 255.0*total_images]
    return average_color,std_color



def get_dataset_labels(dataset_name=""):
    labels =[]
    if(dataset_name=="gehler"):
        file = open("/content/gdrive/My Drive/VISAO/data/k_means_results/groundtruth_568_labels.txt", "r")
        for line in file:
            labels.append(line.split()[1])

    print (labels)
    return np.asarray(labels)


def get_mcc_coord(img_name):
    # Note: relative coord
    with open('./data/Gehler_dataset/coordinates/' + img_name +
              '_macbeth.txt', 'r') as f:
      lines = f.readlines()
      width, height = map(float, lines[0].split())
      scale_x = 1 / width
      scale_y = 1 / height
      lines = [lines[1], lines[2], lines[4], lines[3]]
      polygon = []
      for line in lines:
        line = line.strip().split()
        x, y = (scale_x * float(line[0])), (scale_y * float(line[1]))
        polygon.append((x, y))
      return np.array(polygon, dtype='float32')


def apply_black_mask(img_dir,img_names_list):
    """colourchecker_gamma1_bit12.mat -> (568,24,8) contem as coodernadas da paleta de cores"""
    colourchecker = scipy.io.loadmat('./data/Gehler_dataset/groundtruth_568/colourchecker_gamma1_bit12.mat')['real_rgb']
    for k in range(0,len(img_names_list)):
        img_name = img_names_list[k][0][0]
        end = img_name.find('.png', 0)  # tirar o .png
        img_name = img_name[0:end]

        # img = cv2.cvtColor(cv2.imread(img_dir + "/" + img_name + '.tif'), cv2.COLOR_BGR2RGB)
        img = cv2.imread(img_dir + "/" + img_name + '.tif')

        mcc_coord = get_mcc_coord(img_name)
        polygon = []
        """Transformando pra escalas reais"""
        for coord in mcc_coord:
            x, y = (coord[0] * float(img.shape[1])), (coord[1] * float(img.shape[0]))
            polygon.append((x, y))

        polygon = np.array(polygon, dtype='float32')
        polygon = polygon.astype(np.int32)
        img = cv2.fillPoly(img, [polygon], (1e-5,)*3)

        cv2.imwrite('./data/Gehler_dataset/input/'+img_name+'.tif',img)


def get_names_and_labels(fileName):
    img_names_list = []
    labels = []
    with open(fileName, 'r') as file:
        for line in file:
            line = line.strip().split()
            img_names_list.append(line[0])
            labels.append(int(line[1]))

        return np.asarray(img_names_list), np.asarray(labels)

def get_names_and_labels_by_id(list_names,list_labels,lst_id):

    lst_names = []
    lst_labels = []

    for id_x in lst_id:
        lst_names.append(list_names[id_x])
        lst_labels.append(list_labels[id_x])

    return np.asarray(lst_names),np.asarray(lst_labels)

#RETORNA AS LABELS, PATCHES 
def get_names_and_labels_patches(list_names, list_labels, qtde_patches):
    
    list_names_patches = []
    list_labels_patches = []
    
    for i in range(len(list_names)):
      for j in range(0, qtde_patches):
        list_names_patches.append(list_names[i]+"_"+'{:03}'.format(j))
        list_labels_patches.append(list_labels[i])
     
    return np.asarray(list_names_patches),np.asarray(list_labels_patches)
    
    
"""imgs_dir = '/content/gdrive/My Drive/VISAO/data/Gehler_dataset/input/'
   imgs_name = 'colorconst_cnn/data/k_means_results/groundtruth_568_labels.txt'
   patches_dir = '/content/gdrive/My Drive/VISAO/data/Gehler_dataset/patches'
"""

def get_groundtruth(groundtruth_file,img_name):
    rg = []
    with open(groundtruth_file, 'r') as file:
        for line in file:
            line = line.strip().split()
            if(img_name == line[0]):
              rg  = [float(line[1]),float(line[2])]
    return rg 
  
def get_centroids(centroids_file,label):
    centroid = []
    with open(centroids_file, 'r') as file:
        for line in file:
            line = line.strip().split()
            if(label == int(line[0])):
              centroid  = [float(line[1]),float(line[2])]
    return centroid 
  
def generate_patches(imgs_dir,imgs_name,patches_dir):
    img_name_list, labels = get_names_and_labels(imgs_name)
    max_patches = 200 #default 200
    last_indice = 0
    list_patches = []
    print(len(img_name_list))
    for i in range(len(img_name_list)):
      print(i)
      last_indice = i
      img = io.imread(imgs_dir+img_name_list[i]+'.tif') 
#       print(imgs_dir+img_name_list[i]+'.tif')
      p_h, p_w = 250,250
      expected_n_patches = max_patches
      extr = PatchExtractor(patch_size=(p_h, p_w), max_patches=max_patches, random_state=0)
      patches = extr.transform(np.asarray([img]))
      
      if not os.path.exists(os.path.join(patches_dir, img_name_list[i])):
            os.mkdir(os.path.join(patches_dir, img_name_list[i]))
          
      listimag = os.listdir(os.path.join(patches_dir, img_name_list[i])) # dir is your directory path
      
      number_files = len(listimag)  
      if(number_files<max_patches):
        print('gerando patches faltantes')
        index = 0
        for patch in patches:
          io.imsave(os.path.join(patches_dir,img_name_list[i],img_name_list[i]+"_"+'{:03}'.format(index)+'.tif'),patch.astype('uint8'))
          index += 1
    
        
      
        
        
def save_patches(patches_dir,img_names):
  
  
  img_name_list, labels = get_names_and_labels('/content/gdrive/My Drive/VISAO/data/k_means_results/groundtruth_568_labels.txt')
  print(img_name_list[0])
  dataset_list = glob.glob(img_names) 
  img = io.imread(dataset_list[0])
  io.imshow(img)
# def init_params(net):
#     '''Init layer parameters.'''
#     for m in net.modules():
#         if isinstance(m, nn.Conv2d):
#             init.kaiming_normal(m.weight, mode='fan_out')
#             if m.bias:
#                 init.constant(m.bias, 0)
#         elif isinstance(m, nn.BatchNorm2d):
#             init.constant(m.weight, 1)
#             init.constant(m.bias, 0)
#         elif isinstance(m, nn.Linear):
#             init.normal(m.weight, std=1e-3)
#             if m.bias:
#                 init.constant(m.bias, 0)


# _, term_width = os.popen('stty size', 'r').read().split()
# term_width = int(term_width)
#
# TOTAL_BAR_LENGTH = 65.
# last_time = time.time()
# begin_time = last_time


def progress_bar(current, total, msg=None):
    global last_time, begin_time
    if current == 0:
        begin_time = time.time()  # Reset for new bar.

    cur_len = int(TOTAL_BAR_LENGTH*current/total)
    rest_len = int(TOTAL_BAR_LENGTH - cur_len) - 1

    sys.stdout.write(' [')
    for i in range(cur_len):
        sys.stdout.write('=')
    sys.stdout.write('>')
    for i in range(rest_len):
        sys.stdout.write('.')
    sys.stdout.write(']')

    cur_time = time.time()
    step_time = cur_time - last_time
    last_time = cur_time
    tot_time = cur_time - begin_time

    L = []
    L.append('  Step: %s' % format_time(step_time))
    L.append(' | Tot: %s' % format_time(tot_time))
    if msg:
        L.append(' | ' + msg)

    msg = ''.join(L)
    sys.stdout.write(msg)
    for i in range(term_width-int(TOTAL_BAR_LENGTH)-len(msg)-3):
        sys.stdout.write(' ')

    # Go back to the center of the bar.
    for i in range(term_width-int(TOTAL_BAR_LENGTH/2)+2):
        sys.stdout.write('\b')
    sys.stdout.write(' %d/%d ' % (current+1, total))

    if current < total-1:
        sys.stdout.write('\r')
    else:
        sys.stdout.write('\n')
    sys.stdout.flush()


def format_time(seconds):
    days = int(seconds / 3600/24)
    seconds = seconds - days*3600*24
    hours = int(seconds / 3600)
    seconds = seconds - hours*3600
    minutes = int(seconds / 60)
    seconds = seconds - minutes*60
    secondsf = int(seconds)
    seconds = seconds - secondsf
    millis = int(seconds*1000)

    f = ''
    i = 1
    if days > 0:
        f += str(days) + 'D'
        i += 1
    if hours > 0 and i <= 2:
        f += str(hours) + 'h'
        i += 1
    if minutes > 0 and i <= 2:
        f += str(minutes) + 'm'
        i += 1
    if secondsf > 0 and i <= 2:
        f += str(secondsf) + 's'
        i += 1
    if millis > 0 and i <= 2:
        f += str(millis) + 'ms'
        i += 1
    if f == '':
        f = '0ms'
    return f


def get_name_labels_imagenet(file_dir,root_dir):
    list_img_names = []
    list_img_labels = []

    with open(file_dir, 'r') as file:
        for line in file:
            line = line.strip().split()
            img_name = os.path.join(root_dir, line[0] + '.JPEG')
            image = io.imread(img_name)
            if(len(image.shape)==3):
              list_img_names.append(line[0])
              list_img_labels.append(line[0][0:line[0].index('/')])

    return list_img_names, list_img_labels
# save_patches('/content/gdrive/My Drive/VISAO/data/Gehler_dataset/new/*.mat','/content/gdrive/My Drive/VISAO/data/Gehler_dataset/input/*.tif')
# generate_patches('/content/gdrive/My Drive/VISAO/data/Gehler_dataset/input/','colorconst_cnn/data/k_means_results/groundtruth_568_labels.txt',
#                 '/content/gdrive/My Drive/VISAO/data/Gehler_dataset/patches')
