from __future__ import print_function, division
import os, datetime, time
import torch
import pandas as pd
from skimage import io, transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
# Ignore warnings
import warnings
warnings.filterwarnings("ignore")
import pickle
import scipy.io
import PIL
import utils
from utils import progress_bar
# from utils2 import progress_bar
# import clustering
import torch.nn as nn
import torch.optim as optim
# from model import ColorNet 
from colornet import ColorNet
from sklearn.model_selection import StratifiedKFold
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
import argparse
import re
import glob
import cv2
import random
from custom_transform import NRandomCrop
from gradcam import apply_gradcam
from sklearn import preprocessing

plt.ion()   # interactive mode

parser = argparse.ArgumentParser(description='Color Constancy')
parser.add_argument('--lr', default=1e-2, type=float, help='learning rate')
parser.add_argument('--epochs','-e', default=10, type=int, help='total de epocas')
parser.add_argument('--interval','-i', default=3, type=int, help='define de quantas em quantas epocas sera salvo o modelo ')
parser.add_argument('--resume', '-r', action='store_true', help='resume from checkpoint')
parser.add_argument('--seed', type=int, default=0, metavar='S', help='random seed (default: 1)') 
parser.add_argument('--model_dir', default='colorconst_cnn/mean_models', help='directory of the model')
parser.add_argument('--model', default='ColorNet_MAP', type=str, help='the model name')
parser.add_argument('--indexes_dir', default='/content/drive/My Drive/VISAO/data/Gehler_dataset/train_indexes', type=str, help='the model name')
parser.add_argument('--folder', type=int, default=1, metavar='S', help='random seed (default: 1)')
parser.add_argument('--batch_size', default=5, type=int, help='batch size')

args = parser.parse_args()
torch.manual_seed(args.seed)

device = 'cuda' if torch.cuda.is_available() else 'cpu'
best_acc = 0  # best test accuracy
aux_save = 0 #var contadora 0..inter
last_epoch = 0 

save_dir = os.path.join('colorconst_cnn/mean_models', args.model+'_' + 'folder' + str(args.folder))

if not os.path.exists(save_dir):
    os.mkdir(save_dir)

random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed)
cudnn.benchmark=False
cudnn.deterministic=True


def worker_init(worker_id):
    random.seed(args.seed)
######################################################################

class ColorImageDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, img_names_list,labels, transform=None, transform_crop=None):

        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem          
        """
        self.root_dir = root_dir
        self.img_names_list = img_names_list
        self.labels = labels
        
        self.transform = transform
        self.transform_crop = transform_crop

    def __len__(self):
        return len(self.img_names_list)

    def __getitem__(self, idx):
      

        img_name = os.path.join(self.root_dir, self.img_names_list[idx]+'.tif')        
        
        image = io.imread(img_name)
        image = PIL.Image.fromarray(image)
        
        crops = image.copy() #crops sao os recortes sem transformacoes de rotacao, etc
        patches = image.copy()#patches sao os crops com transformacoes de rotacao, etc
        label = self.labels[idx]
                
#         image_name = self.img_names_list[idx]
       
        if self.transform_crop:
            crops, points = self.transform_crop(crops) #self.transform_crop pega a imagem original (PIL image) e faz um NRandomCrop, retorna uma list
            points =  np.asarray(points)
            patches = crops.copy()      #patches recebe um clone da lista de imagens (pil)      
            crops = torch.stack([transforms.ToTensor()(crop) for crop in crops]) #converto cada crop em um tensor
            
#             print('crops|patch',crops.shape)
            
        if self.transform:
          
            patches = torch.stack([ self.transform(patch) for patch in patches]) #aplico a cada crop as transformacoes, tranformando em tensor no final          
#             print(patches.shape)

#         labels = [self.labels[idx] for i in range(patches.shape[0])]
#         labels = torch.stack([ self.labels[idx]  for patch in patches]) 
#         print('PATCHE SHAPE',len(labels),i)
#         labels = transforms.ToTensor()(np.asarray(labels))
        sample = {'crops': crops, 'patches': patches, 'label': label, 'image_name': img_name, 'points': points}

    
        return sample
      
######################################################################

def findLastCheckpoint(save_dir):
    file_list = glob.glob(os.path.join(save_dir, 'model_*.pth'))
    if file_list:
        epochs_exist = []
        for file_ in file_list:
            result = re.findall(".*model_(.*).pth.*", file_)
            epochs_exist.append(int(result[0]))
        initial_epoch = max(epochs_exist)
    else:
        initial_epoch = 0
    return initial_epoch
######################################################################

def split(img_name_list, labels):
  
  skf = StratifiedKFold(n_splits=3)
  train_name_list = []
  train_label_list = []
  test_name_list = []
  test_label_list = []

  for train, test in skf.split(img_name_list, labels):
      # print("%d %d" % (len(train), len(test)))
      train_names,train_labels = utils.get_names_and_labels_by_id(img_name_list, labels, train)
      train_name_list.append(train_names)
      train_label_list.append(train_labels)

      test_names, test_labels = utils.get_names_and_labels_by_id(img_name_list, labels, test) 
      test_name_list.append(test_names)
      test_label_list.append(test_labels)

      
  train_name_list = np.asarray(train_name_list)
  train_label_list = np.asarray(train_label_list)
  test_name_list = np.asarray(test_name_list)
  test_label_list = np.asarray(test_label_list)

#   print (train_name_list[0][0],train_label_list[0][0])    
  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/train_indexes','train_set'+str(1)+'.mat'),{'train_names':train_name_list[0],'train_labels':train_label_list[0]})
  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/test_indexes','test_set'+str(1)+'.mat'),{'test_names':test_name_list[0],'test_labels':test_label_list[0]})

  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/train_indexes','train_set'+str(2)+'.mat'),{'train_names':train_name_list[1],'train_labels':train_label_list[1]})
  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/test_indexes','test_set'+str(2)+'.mat'),{'test_names':test_name_list[1],'test_labels':test_label_list[1]})

  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/train_indexes','train_set'+str(3)+'.mat'),{'train_names':train_name_list[2],'train_labels':train_label_list[2]})
  scipy.io.savemat(os.path.join('colorconst_cnn/data/Gehler_dataset/test_indexes','test_set'+str(3)+'.mat'),{'test_names':test_name_list[2],'test_labels':test_label_list[2]})

######################################################################      

def calculate_weights(image_map, crops_points):
 
  crops_points = crops_points.numpy()
#   print('calculate_weights', type(image_map), type(crops_points),image_map.shape, crops_points.shape)
  weights = []
  for crop in crops_points:    
    im = image_map[crop[0]:crop[2], crop[1]:crop[3]]
    weights.append(np.average(im))
    
  weights = np.asarray(weights) 
  weights = torch.from_numpy(weights)
  weights = weights / weights.sum(0).expand_as(weights)

  return weights
  
# Training
def training(epoch):
    print('\nEpoch: %d' % epoch)
    net.train()
    train_loss = 0
    correct = 0
    total = 0
    batch_loss = 0
           
    for batch_idx, sample_batched in enumerate(trainloader):
        inputs, targets = sample_batched['patches'].to(device), sample_batched['label'].to(device) 
        
        points = sample_batched['points'] #shape = (batch_size, crops_num, 4)
                
        cam_list = apply_gradcam(sample_batched['image_name'])
        targets_int = targets.cpu()
        targets_int = targets_int.data.numpy() #convert tensor to numpy array
        
        mean_outputs = torch.zeros(4)
        mean = []
        optimizer.zero_grad()
        for i in range(inputs.shape[0]):
    
              
            patches = inputs[i]
            crops_points = points[i]
            image_map = cam_list[i]
    
            weights = calculate_weights(image_map, crops_points)            
            
            outputs = net(patches)  
           
            """Ponderar saidas de acordo com o peso de cada patch"""
            outputs = outputs.type(torch.FloatTensor)
            weights = weights.type(torch.FloatTensor)#            
            weights = weights.unsqueeze(1)
            wei_sum = outputs*weights
#             print('out_a[1]', out_a[1])
            
            """Calculo a media da saidas ja ponderadas pra cada patch"""
            wei_sum = torch.mean(wei_sum,dim=0)
            """scale to range(0,1)"""
            wei_sum = wei_sum / wei_sum.sum(0).expand_as(wei_sum)
            mean.append(wei_sum.detach().numpy())

        mean_tensor = torch.from_numpy(np.asarray(mean))
        mean_tensor = Variable(mean_tensor, requires_grad=True)

        
#         mean_outputs = torch.from_numpy(np.asarray(mean_outputs)).to(device)
#         print('mean shape: ', mean_tensor.shape)        

        mean_tensor = mean_tensor.to(device)     
        loss = criterion(mean_tensor, targets)   

        loss.backward()
        optimizer.step()

        train_loss += loss.item()            
        _, predicted = mean_tensor.max(1)
        total += targets.size(0)
        correct += predicted.eq(targets).sum().item()

        batch_loss = train_loss/(batch_idx+1)
        if batch_idx % 10 == 0:
#             print (total,correct,100.*(correct / total))
            print('%4d %4d / %4d loss = %2.4f acc = %2.4f' % (epoch, batch_idx, len(train_names)//args.batch_size, loss.item()/args.batch_size ,100.*(correct / total)))

          
    torch.save(net, os.path.join(save_dir, 'model_%03d.pth' % (epoch+1)))
             
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PREPARING DATA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

print('==> Preparing data..')


gehler = {'root_dir': '/content/drive/My Drive/VISAO/data/Gehler_dataset/input_resized',#'root_dir': 'colorconst_cnn/data/Gehler_dataset/patches',
            'img_names_labels':'colorconst_cnn/data/mean_shift_results/groundtruth_568_labels.txt'
        }

img_name_list, labels = utils.get_names_and_labels(gehler['img_names_labels'])
# print(len(img_name_list),len(labels))
# print(img_name_list[0],labels[0])


"""split(): Aqui geramos os indices para treinamento e teste"""
# split(img_name_list, labels)

# print(img_name_list[567], labels[567])

# average_color,std_color = utils.get_mean_stDeviation(gehler['root_dir'],img_name_list)

average_color = (0.28085261,0.2558284,0.21423263)
std_color = (0.19844685,0.18925277,0.18685587)


data_transform = transforms.Compose([ transforms.RandomRotation((-10,10)),transforms.RandomRotation((-5,5)),
        transforms.RandomRotation(0),transforms.RandomRotation(5),transforms.RandomRotation(10),
        # transforms.RandomCrop(227),
        transforms.RandomAffine(0, scale=(250,1000)),
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(average_color, std_color),
    ])

crop_transform  = transforms.Compose(
    [NRandomCrop(size=227, n=50, padding=4),
#      transforms.Lambda(
#          lambda crops: torch.stack([transforms.ToTensor()(crop) for crop in crops])),

     ])



train_names = scipy.io.loadmat(os.path.join(args.indexes_dir,'train_set'+str(args.folder)+'.mat'))['train_names']
train_labels = scipy.io.loadmat(os.path.join(args.indexes_dir,'train_set'+str(args.folder)+'.mat'))['train_labels'][0] #[[]]
# print (train_names[0], train_labels[0])
# print(len(train_names),len(train_labels))


train_dataset = ColorImageDataset(root_dir=gehler['root_dir'], img_names_list=train_names,
                                      labels=train_labels, transform=data_transform, transform_crop=crop_transform)

trainloader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=4, worker_init_fn=worker_init)


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BUILDING MODEL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

print('==> Building model..')

net = ColorNet(K=4)
"""negative log likelihood loss"""
criterion = nn.NLLLoss()
# optimizer = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9, weight_decay=5e-4)
initial_epoch = findLastCheckpoint(save_dir=save_dir)  # load the last model in matconvnet style

if initial_epoch > 0:
    print('resuming by loading epoch %03d' % initial_epoch)
    # model.load_state_dict(torch.load(os.path.join(save_dir, 'model_%03d.pth' % initial_epoch)))
    model = torch.load(os.path.join(save_dir, 'model_%03d.pth' % initial_epoch))
else:
  print('Comecando novo treino...')
        
optimizer = optim.SGD([{'params': net.features[0].parameters(),'lr': 10*args.lr},
                       {'params': net.features[4].parameters()},
                       {'params': net.features[8].parameters()}, 
                       {'params': net.features[10].parameters()}, 
                       {'params': net.features[12].parameters()}, 
                       {'params': net.classifier[0].parameters()},
                       {'params': net.classifier[3].parameters(),'lr': 10*args.lr},
                       {'params': net.classifier[6].parameters(),'lr': 10*args.lr}
                      ], lr=args.lr, momentum=0.9, weight_decay=5e-4)

net = net.to(device)
if device == 'cuda':
  net = torch.nn.DataParallel(net)
  cudnn.benchmark = True
 


for epoch in range(initial_epoch, args.epochs):
    #print('epoch',epoch)
    start_time = time.time()
    training(epoch)
    elapsed_time = time.time() - start_time
    print('>>>epoch = %4d | time = %2.4f'%(epoch,elapsed_time))
