# -*- coding: utf-8 -*-
import datasets
import k_means
import pickle
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import MeanShift

def save_result(list_images_name,dataset_name,labels,centroids):
    f = open('data/k_means_results/'+dataset_name+"_labels.txt", "w+")
    for i in range(len(list_images_name)):
        img_name = list_images_name[i][0][0]
        end = img_name.find('.png', 0)  # tirar o .png
        img_name = img_name[0:end]

        f.write(img_name+" "+str(labels[i])+"\n")
    f.close()
    f2 = open('data/k_means_results/' + dataset_name + "_centroids.txt", "w+")
    for i in range(0,centroids.shape[0]):
        f2.write(str(i)+" "+str(centroids[i][0])+" "+str(centroids[i][1])+"\n")
    f2.close()

def save_result2(list_images_name,dataset_name,labels,centroids):
    f = open('data/mean_shift_results/'+dataset_name+"_labels.txt", "w+")
    for i in range(len(list_images_name)):
        img_name = list_images_name[i]
        # end = img_name.find('.png', 0)  # tirar o .png
        # img_name = img_name[0:end]

        f.write(img_name+" "+str(labels[i])+"\n")
    f.close()
    f2 = open('data/mean_shift_results/' + dataset_name + "_centroids.txt", "w+")
    for i in range(0,centroids.shape[0]):
        f2.write(str(i)+" "+str(centroids[i][0])+" "+str(centroids[i][1])+"\n")
    f2.close()

def clustering_illuminants(list_images_name,database_name='groundtruth_568',plot_result=False):

    k_dic={'gehler':25,'gray-ball':20,'nus8':50}


    data,_ = datasets.get_rg_chromaticity(database_name=database_name)
    """clusters = (568,)
    clusters_centroids = (K,2)"""
    clusters, clusters_centroids = k_means.apply_kmeans(data,k_dic['gehler'])
    save_result(list_images_name,database_name, clusters,clusters_centroids)

    ##############################################################################
    # Plot result
    if (plot_result):

        r_ch = data[:, data.shape[1] - 2]
        g_ch = data[:, data.shape[1] - 1]

        # clusters_centroids = np.asarray(clusters_centroids)
        x_centroid = clusters_centroids[:, clusters_centroids.shape[1] - 2]
        y_centroid = clusters_centroids[:, clusters_centroids.shape[1] - 1]

        cmap = plt.get_cmap('viridis')
        colors = cmap(np.linspace(0, 1, k_dic['gehler']))

        color = '#4EACC5'

        plt.figure()
        plt.hold(True)

        for k in range(k_dic['gehler']):
            # for i, (name, color) in enumerate(zip(names, colors), 1):
            # my_members = k_means_labels == k
            # cluster_center = k_means_cluster_centers[k]
            plt.plot(r_ch, g_ch, 'w', markeredgecolor=color, marker='.')
            plt.plot(x_centroid, y_centroid, 'o', markerfacecolor=color,
                     markeredgecolor='k', markersize=6)
        plt.title('KMeans')
        plt.grid(True)
        plt.show()

def clustering_illuminants2(database_name='groundtruth_568', plot_result=True):
    from sklearn.cluster import MeanShift, estimate_bandwidth
    from sklearn.datasets.samples_generator import make_blobs

    k_dic={'gehler':25,'gray-ball':20,'nus8':50}


    data,list_images_name = datasets.get_rg_chromaticity(database_name=database_name)

    # bandwidth = estimate_bandwidth(data, quantile=0.2, n_samples=568)
    data = np.asarray(data)
    # ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
    ms = MeanShift()
    ms.fit(data)
    labels = ms.labels_
    cluster_centers = ms.cluster_centers_

    labels_unique = np.unique(labels)
    n_clusters_ = len(labels_unique)
    save_result2(list_images_name, database_name, labels, cluster_centers)
    print("number of estimated clusters : %d" % n_clusters_)

    # #############################################################################
    # Plot result
    import matplotlib.pyplot as plt
    from itertools import cycle

    plt.figure(1)
    plt.clf()

    colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
    for k, col in zip(range(n_clusters_), colors):
        my_members = labels == k
        cluster_center = cluster_centers[k]
        plt.plot(data[my_members, 0], data[my_members, 1], col + '.')
        plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
                 markeredgecolor='k', markersize=14)
    plt.title('Estimated number of clusters: %d' % n_clusters_)
    plt.show()



    # """clusters = (568,)
    # clusters_centroids = (K,2)"""
    #
    # data = np.asarray(data)
    # clustering = MeanShift().fit(data)
    # labels = clustering.labels_
    # clusters_centroids = clustering.cluster_centers_
    # # clusters, clusters_centroids = k_means.apply_kmeans(data,k_dic['gehler'])
    # # save_result(list_images_name,database_name, clusters,clusters_centroids)
    #
    # ##############################################################################
    # # Plot result
    # if (plot_result):
    #
    #     r_ch = data[:, data.shape[1] - 2]
    #     g_ch = data[:, data.shape[1] - 1]
    #
    #     # clusters_centroids = np.asarray(clusters_centroids)
    #     x_centroid = clusters_centroids[:, clusters_centroids.shape[1] - 2]
    #     y_centroid = clusters_centroids[:, clusters_centroids.shape[1] - 1]
    #
    #     cmap = plt.get_cmap('viridis')
    #     colors = cmap(np.linspace(0, 1, k_dic['gehler']))
    #
    #     color = '#4EACC5'
    #
    #     plt.figure()
    #     plt.hold(True)
    #
    #     for k in range(k_dic['gehler']):
    #         # for i, (name, color) in enumerate(zip(names, colors), 1):
    #         # my_members = k_means_labels == k
    #         # cluster_center = k_means_cluster_centers[k]
    #         plt.plot(r_ch, g_ch, 'w', markeredgecolor=color, marker='.')
    #         plt.plot(x_centroid, y_centroid, 'o', markerfacecolor=color,
    #                  markeredgecolor='k', markersize=6)
    #     plt.title('KMeans')
    #     plt.grid(True)
    #     plt.show()


clustering_illuminants2()
