from __future__ import print_function, division
import os, datetime, time
import torch
import pandas as pd
from skimage import io, transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
# Ignore warnings
import warnings
warnings.filterwarnings("ignore")
import pickle
import scipy.io
import PIL
import utils
from utils import progress_bar
import torch.nn as nn
import torch.optim as optim
# from model import ColorNet
from colornet import ColorNet
from sklearn.model_selection import StratifiedKFold
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
import argparse
import re
import glob
import random




parser = argparse.ArgumentParser(description='Color Constancy - Pretreinamento')
parser.add_argument('--lr', default=1e-2, type=float, help='learning rate')
parser.add_argument('--epochs','-e', default=10, type=int, help='total de epocas')
parser.add_argument('--interval','-i', default=3, type=int, help='define de quantas em quantas epocas sera salvo o modelo ')
parser.add_argument('--resume', '-r', action='store_true', help='resume from checkpoint')
parser.add_argument('--seed', type=int, default=0, metavar='S', help='random seed (default: 1)')
# parser.add_argument('--model_dir', default='colorconst_cnn/mean_models', help='directory of the model')
parser.add_argument('--model', default='ColorNet_ImageNet', type=str, help='the model name')
parser.add_argument('--root_dir', default='/media/dsc4/HD21/ImageNet/ILSVRC/Data/CLS-LOC/train', type=str, help='train images dir')
# parser.add_argument('--indexes_dir', default='colorconst_cnn/data/Gehler_dataset/train_indexes', type=str, help='the model name')
# parser.add_argument('--folder', type=int, default=1, metavar='S', help='random seed (default: 1)')
parser.add_argument('--batch_size', default=100, type=int, help='batch size')

args = parser.parse_args()
torch.manual_seed(args.seed)
best_acc = 0  # best test accuracy
aux_save = 0 #var contadora 0..inter
last_epoch = 0

save_dir = os.path.join('pretrained_models')

if not os.path.exists(save_dir):
    os.mkdir(save_dir)

random.seed(args.seed)
np.random.seed(args.seed)
torch.manual_seed(args.seed)
torch.cuda.manual_seed(args.seed)
cudnn.benchmark=False
cudnn.deterministic=True
def worker_init(worker_id):
    random.seed(args.seed)

device = 'cuda' if torch.cuda.is_available() else 'cpu'

####################################################
class ColorImageDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, img_names_list, labels, transform=None):
        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem
        """
        self.root_dir = root_dir
        self.img_names_list = img_names_list
        self.labels = labels
        self.transform = transform

    def __len__(self):
        return len(self.img_names_list)

    def __getitem__(self, idx):
        # original_image = self.img_names_list[idx][:-4]

        self.img_names_list[idx] = self.img_names_list[idx].strip()

        img_name = os.path.join(self.root_dir, self.img_names_list[idx] + '.JPEG')

        image = io.imread(img_name)

        image = PIL.Image.fromarray(image)

        label = self.labels[idx]

        sample = {'image': image, 'label': label}

        if self.transform:
            sample['image'] = self.transform(sample['image'])

        return sample


######################################################################
def findLastCheckpoint(save_dir):
    file_list = glob.glob(os.path.join(save_dir, 'model_*.pth'))
    if file_list:
        epochs_exist = []
        for file_ in file_list:
            result = re.findall(".*model_(.*).pth.*", file_)
            epochs_exist.append(int(result[0]))
        initial_epoch = max(epochs_exist)
    else:
        initial_epoch = 0
    return initial_epoch




######################################################################
""">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>PREPARING DATA<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"""


print('==> Preparing data..')


# average_color,std_color = utils.get_mean_stDeviation(gehler['root_dir'],img_name_list)

#https://github.com/facebook/fb.resnet.torch/issues/78
average_color = (0.485, 0.456, 0.406)
std_color = ( 0.229, 0.224, 0.225)


data_transform = transforms.Compose([
        # transforms.RandomCrop(100),
        # transforms.Resize(256),
        transforms.RandomResizedCrop(227),
        transforms.ToTensor(),
        transforms.Normalize(average_color, std_color),
    ])

train_names = scipy.io.loadmat('train_img.mat')['train_img']
train_labels = scipy.io.loadmat('train_img.mat')['train_labels']


# #Como na base imagenet algumas imagens estao em cinza, foi necessario fazer um filtro.
# train_names, train_labels = utils.get_name_labels_imagenet("/media/dsc4/HD21/ImageNet/ILSVRC/ImageSets/CLS-LOC/train_loc.txt",root_dir = args.root_dir)
# scipy.io.savemat(os.path.join('train_img.mat'),{'train_img': train_names, 'train_labels':train_labels})
# labels_uniq = sorted(set(train_labels.tolist()), key=train_labels.tolist().index)
#
# train_labels_ints = []
# for i in range(1,len(train_labels)+1):
#     train_labels_ints.append(labels_uniq.index(train_labels[i-1])+1)
# print(max(train_labels_ints))
# scipy.io.savemat(os.path.join('train_img.mat'),{'train_img': train_names, 'train_labels':train_labels_ints})

# print(len(train_names),len(train_labels))

train_labels = np.reshape(train_labels,(train_labels.shape[1],))


train_dataset = ColorImageDataset(root_dir="/media/dsc4/HD21/ImageNet/ILSVRC/Data/CLS-LOC/train",
                                  img_names_list=train_names, labels=train_labels, transform=data_transform)

trainloader = DataLoader(train_dataset, batch_size=args.batch_size, shuffle=True, num_workers=4,
                         worker_init_fn=worker_init)


##########################Training############################################
def training(epoch):
    print('\nEpoch: %d' % epoch)
    net.train()
    train_loss = 0
    correct = 0
    total = 0
    batch_loss = 0


    for batch_idx, sample_batched in enumerate(trainloader):
        inputs, targets = sample_batched['image'].to(device), sample_batched['label'].to(device)
        optimizer.zero_grad()
        outputs = net(inputs)
        loss = criterion(outputs, targets)
        loss.backward()
        optimizer.step()

        train_loss += loss.item()
        _, predicted = outputs.max(1)
        total += targets.size(0)
        correct += predicted.eq(targets).sum().item()

        batch_loss = train_loss / (batch_idx + 1)
        if batch_idx % 10 == 0:
            print('%4d %4d / %4d loss = %2.4f acc = %2.4f' % (
                epoch, batch_idx, len(train_names) // args.batch_size, batch_loss,
                100. * (correct / total)))

    torch.save(net, os.path.join(save_dir, 'model_%03d.pth' % (epoch + 1)))


"""">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BUILDING MODEL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"""

print('==> Building model..')

net = ColorNet(K=1000, pretrain=True)
print(net)

"""negative log likelihood loss"""
criterion = nn.NLLLoss()
# optimizer = optim.SGD(net.parameters(), lr=args.lr, momentum=0.9, weight_decay=5e-4)
initial_epoch = findLastCheckpoint(save_dir=save_dir)  # load the last model in matconvnet style
print('save_dir',save_dir)
optimizer = optim.SGD([{'params': net.features[0].parameters(), 'lr': 10 * args.lr},
                       {'params': net.features[1].parameters()},
                       {'params': net.features[2].parameters()},
                       {'params': net.features[3].parameters()},
                       {'params': net.features[4].parameters()},
                       {'params': net.features[5].parameters()},
                       {'params': net.features[6].parameters()},
                       {'params': net.features[7].parameters()},
                       {'params': net.features[8].parameters()},
                       {'params': net.features[9].parameters()},
                       {'params': net.features[10].parameters()},
                       {'params': net.features[11].parameters()},
                       {'params': net.features[12].parameters()},
                       {'params': net.features[13].parameters()},
                       {'params': net.features[14].parameters()},
                       {'params': net.classifier[0].parameters()},
                       {'params': net.classifier[1].parameters()},
                       {'params': net.classifier[2].parameters()},
                       {'params': net.classifier[3].parameters(), 'lr': 10 * args.lr},
                       {'params': net.classifier[4].parameters()},
                       {'params': net.classifier[5].parameters()},
                       {'params': net.classifier[6].parameters(), 'lr': 10 * args.lr}
                       ], lr=args.lr, momentum=0.9, weight_decay=5e-4)


net = net.to(device)
if device == 'cuda':
    net = torch.nn.DataParallel(net)

if initial_epoch > 0:
    print('resuming by loading epoch %03d' % initial_epoch)
    # model.load_state_dict(torch.load(os.path.join(save_dir, 'model_%03d.pth' % initial_epoch)))
    net = torch.load(os.path.join(save_dir, 'model_%03d.pth' % initial_epoch))
    if(net==None):
      print("epa")
else:
    print('Comecando novo treino...')

# optimizer = optim.SGD([{'params': net.features[0].parameters(), 'lr': 10 * args.lr},
#                        {'params': net.features[4].parameters()},
#                        {'params': net.features[8].parameters()},
#                        {'params': net.features[10].parameters()},
#                        {'params': net.features[12].parameters()},
#                        {'params': net.classifier[0].parameters()},
#                        {'params': net.classifier[3].parameters(), 'lr': 10 * args.lr},
#                        {'params': net.classifier[6].parameters(), 'lr': 10 * args.lr}
#                        ], lr=args.lr, momentum=0.9, weight_decay=5e-4)


for epoch in range(initial_epoch, args.epochs):
    # print('epoch',epoch)
    start_time = time.time()
    training(epoch)
    elapsed_time = time.time() - start_time
    print('>>>epoch = %4d | time = %2.4f' % (epoch, elapsed_time))











