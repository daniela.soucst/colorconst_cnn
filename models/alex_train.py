# -*- coding: utf-8 -*-
from __future__ import print_function, division
import os, datetime, time
import torch
import pandas as pd
from skimage import io, transform
import numpy as np
import matplotlib.pyplot as plt
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
# Ignore warnings
import warnings
warnings.filterwarnings("ignore")
import pickle
import scipy.io
import PIL
import utils
from utils import progress_bar
# from utils2 import progress_bar
# import clustering
import torch.nn as nn
import torch.optim as optim
# from model import ColorNet 
from colornet import ColorNet
from sklearn.model_selection import StratifiedKFold
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
import argparse
import re
import glob
import cv2
import random
import torchvision.models as models


# >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BUILDING MODEL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

print('==> Building model..')

# net = ColorNet()
net = models.alexnet(pretrained=True)





