import torch.nn as nn
import torchvision.models as models
import torch.nn.functional as F


######################################################################
class ColorNet(nn.Module):
    # K é a qtde de clusters de iluminação
    def __init__(self, K, pretrain=False):
        super(ColorNet, self).__init__()
        """When pretrain==True then K must be equal 1000"""
        self.k = K
        self.pretrain = pretrain

        self.features = nn.Sequential(
            nn.Conv2d(3, 96, kernel_size=11, stride=4, padding=0),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2, padding=0),
            nn.LocalResponseNorm(2),

            nn.Conv2d(96, 256, kernel_size=5, stride=1, padding=2),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2, padding=0),
            nn.LocalResponseNorm(2),

            nn.Conv2d(256, 384, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(384, 384, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.Conv2d(384, 256, kernel_size=3, stride=1, padding=1),
            nn.ReLU(inplace=True),
            nn.MaxPool2d(kernel_size=3, stride=2),

        )

        self.classifier = nn.Sequential(
            nn.Linear(6 * 6 * 256, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Linear(4096, 4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),
            nn.Linear(4096, self.k),
        )


        if (self.pretrain==False):
            self._initialize_weights()  # carrega pesos da alexnet treinada no imagenet

    def forward(self, x):
        x = self.features(x)
        x = x.view(x.size(0), 256 * 6 * 6)
        x = self.classifier(x)
        return F.softmax(x, dim=1)  # out.shape == (batch_size,K)

    def _initialize_weights(self):
        alex_model = models.alexnet(pretrained=True)

        #         print(alex_model.features[10].weight.data.shape,self.features[12].shape,self.features[10].shape)

        self.features[0].weight.data[:64, :, :, :] = alex_model.features[0].weight  # pegando pesos das convs
        self.features[4].weight.data[:192, :64, :, :] = alex_model.features[3].weight
        self.features[8].weight.data[:384, :192, :, :] = alex_model.features[6].weight
        self.features[10].weight.data[:256, :, :, :] = alex_model.features[8].weight
        self.features[12].weight.data[:256, :256, :, :] = alex_model.features[10].weight

        self.classifier[0].weight = alex_model.classifier[1].weight  # pegando pesos das fcs
        self.classifier[3].weight = alex_model.classifier[4].weight
        self.classifier[6].weight.data[:self.k, :] = alex_model.classifier[6].weight.data[:self.k, :]
