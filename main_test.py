# -*- coding: utf-8 -*-
import argparse
import torch
from colornet import ColorNet
import os, glob, datetime, time
from skimage import io, transform
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import torch.backends.cudnn as cudnn
import torch.nn as nn
import torch.optim as optim
import utils
import scipy.io
import scipy.optimize
import PIL
# Ignore warnings
import warnings
import numpy as np
import re
import math
warnings.filterwarnings("ignore")


device = 'cuda' if torch.cuda.is_available() else 'cpu'

######################################################################

class ColorImageDataset(Dataset):
    """Color Image dataset."""

    def __init__(self, root_dir, img_names_list,labels, real_illu,transform=None):

        """Args:

            root_dir (string): diretorio onde ficam os arquivos de imagem (ex: data/Gehler_dataset/images)
            img_names_list (numpy array): lista que contem os nomes das imagens em ordem
            illu_list (numpy array):lista que contem a classe para determinada imagem
            real_illu (numpy array)
        """
        self.root_dir = root_dir
        self.img_names_list = img_names_list
        self.labels = labels
        self.transform = transform
        self.real_rgb_list = real_illu



    def __len__(self):
        return len(self.img_names_list)

    def __getitem__(self, idx):
        
        
        original_image = self.img_names_list[idx][:-4]
        
        img_name = os.path.join(self.root_dir,original_image, self.img_names_list[idx]+'.tif')
#         img_name = os.path.join(self.root_dir, self.img_names_list[idx]+'.tif')
        
        image = io.imread(img_name)  
        image = PIL.Image.fromarray(image)
        real_rgb = self.real_rgb_list[idx]
        label = self.labels[idx]
#         list_prob_class = np.zeros((np.max(labels)+1,), dtype=int) #vetor de probabilidades, que deve ser maximo na posicao idx
#         list_prob_class[label] = 1
        
       
        sample = {'image': image, 'label': label ,'real_rgb': real_rgb}
          
        if self.transform:
            sample['image'] = self.transform(sample['image'])

        return sample
      

######################################################################

def get_error(e_a,e_b):
  e_a = np.asarray(e_a).astype(np.float32)
  e_b = np.asarray(e_b).astype(np.float32)
  
  dot = np.linalg.norm(np.dot(e_a,e_b))
  norm_a = np.linalg.norm(e_a)
  norm_b = np.linalg.norm(e_b)
  div = dot/(norm_a*norm_b)
  div = np.clip(div, -1, 1)
  
  erro = np.arccos(div)*180/math.pi
  
  return erro

######################################################################

# Params
parser = argparse.ArgumentParser(description='PyTorch ColorNet')
parser.add_argument('--model', default='ColorNet', type=str, help='choose a type of model')
parser.add_argument('--model_name', default='model_010.pth', type=str, help='the model name')
parser.add_argument('--batch_size', default=200, type=int, help='batch size')
parser.add_argument('--train_data', default='/content/gdrive/My Drive/VISAO/data/Gehler_dataset/patches', type=str, help='path of train data')
parser.add_argument('--epoch', default=10, type=int, help='number of train epoches')
parser.add_argument('--indexes_dir', default='/content/gdrive/My Drive/VISAO/data/Gehler_dataset/test_indexes', type=str, help='the model name')
parser.add_argument('--folder', type=int, default=3, metavar='S', help='random seed (default: 1)')
# parser.add_argument('--model_dir', default='/content/gdrive/My Drive/VISAO/checkpoint', help='directory of the model')
parser.add_argument('--lr', default=1e-2, type=float, help='initial learning rate for Adam')
args = parser.parse_args()

batch_size = args.batch_size
cuda = torch.cuda.is_available()
n_epoch = args.epoch
last_epoch = 0
aux_save = 0
best_acc = 0


# save_dir = os.path.join(args.model_dir, args.model+'_' + str(n_epoch))
# save_dir = os.path.join('/content/gdrive/My Drive/VISAO/checkpoint', args.model+str(args.folder)+'.pth')
save_dir = os.path.join('/content/gdrive/My Drive/VISAO/checkpoint', args.model+'_' + 'folder' + str(args.folder))
print('save_dir:',save_dir)


#>>>>>>>>>>>>>>>>>. Preparing data
gehler = {'root_dir': '/content/gdrive/My Drive/VISAO/data/Gehler_dataset/patches',
            'img_names_labels':'/content/gdrive/My Drive/VISAO/data/k_means_results/groundtruth_568_labels.txt',
            'real_illu_dir':'/content/gdrive/My Drive/VISAO/data/Gehler_dataset/groundtruth_568/real_illum_568.mat',
        }

img_name_list, labels = utils.get_names_and_labels(gehler['img_names_labels'])
real_illu = scipy.io.loadmat(gehler['real_illu_dir'])['real_rgb'] #variavel global que cont�m os centros dos clusters de ilumina��o
# average_color,std_color = utils.get_mean_stDeviation(gehler['root_dir'],img_name_list)
average_color = (0.28085261,0.2558284,0.21423263)
std_color = (0.19844685,0.18925277,0.18685587)



#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>BUILDING MODEL<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
# model selection
print('===> Building model')
net = ColorNet()

net = net.to(device)
if device == 'cuda':
  net = torch.nn.DataParallel(net)
  cudnn.benchmark = True
 
if not os.path.exists(os.path.join(save_dir, args.model_name)):

    net = torch.load(os.path.join(save_dir, 'model.pth'))
    # load weights into new model
    print('load trained model on Train400 dataset by kai')
else:
    # model.load_state_dict(torch.load(os.path.join(args.model_dir, args.model_name)))
    net = torch.load(os.path.join(save_dir, args.model_name))
    print('load trained model: ',os.path.join(save_dir, args.model_name))


def test(test_loader):
    net.eval()
    test_loss = 0
    correct = 0
    weights_list = []
    with torch.no_grad():
      for n_count, batch_yx in enumerate(testloader):
        inputs,targets = batch_yx['image'].to(device), batch_yx['label'].to(device)
  #       print(len(inputs))
        outputs = net(inputs)

  #       weights_list.append(outputs.item())
        weights_list = outputs.mean(0)

      return weights_list.cpu()
#######################################################            
transform_test = transforms.Compose([
    transforms.RandomCrop(227),
    transforms.ToTensor(),
    transforms.Normalize(average_color, std_color),
])


test_names = scipy.io.loadmat(os.path.join(args.indexes_dir,'test_set'+str(args.folder)+'.mat'))['test_names']
test_labels = scipy.io.loadmat(os.path.join(args.indexes_dir,'test_set'+str(args.folder)+'.mat'))['test_labels'][0] #[[]]



test_names, test_labels, real_illu = utils.get_names_and_labels_patches(test_names,test_labels,real_illu,200)

centroids = []
for k in range(0,25):
  centroids.append(utils.get_centroids('/content/gdrive/My Drive/VISAO/data/k_means_results/groundtruth_568_centroids.txt',k))

centroids = np.asarray(centroids)
lst_error = []


# f = open('/content/gdrive/My Drive/VISAO/data/k_means_results/folder'+str(args.folder)+'.txt', "w+")
for i in range(0,len(test_names),200):
  
  print('Testando imagem:'+str(i))
  test_names_folder = test_names[i:i+200]
  test_labels_folder = test_labels[i:i+200]
  real_illu_folder = real_illu[i:i+200]
  
  print(test_names_folder[0])




  test_dataset = ColorImageDataset(root_dir=gehler['root_dir'], img_names_list=test_names_folder,
                                        labels=test_labels_folder, real_illu=real_illu_folder, transform=transform_test)

  testloader = DataLoader(test_dataset, batch_size=args.batch_size, shuffle=True, num_workers=4)   

  #submete imagem teste a rede          
  weights_list = test(testloader)  
  weights_list = weights_list.data.numpy() #convert tensor to numpy array

  #e_a = ilumina��o estimada, a mesma � resultante da m�dia ponderada da saida gerada pela rede pelos centroides 
  e_a = [0.,0.]
  for w in range(len(weights_list)):

    e_a[0] += centroids[w][0]*weights_list[w]
    e_a[1] += centroids[w][1]*weights_list[w]


#   e_b = utils.get_groundtruth('/content/gdrive/My Drive/VISAO/data/k_means_results/srgb8bit_whitepoints.txt',test_names_folder[0][:-4])
  e_b = real_illu_folder[0].tolist() #groundtruth

  #calculamos o erro
  erro = get_error(e_a,e_b)
  lst_error.append(erro)
#     f.write(str(erro)+"\n")
  print(e_a,e_b,erro)

  
f.close()  
print(len(lst_error))
scipy.io.savemat('/content/gdrive/My Drive/VISAO/data/k_means_results/lst_error.mat',{'lst_error':lst_error})
lst_error = sorted(lst_error)
print('Mean:', np.mean(lst_error),np.median(lst_error))